;(function($){

    function urlencode (str) {
      str = (str + '').toString();

      return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
      replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
    }

    function escapeHtml(string) {
        return String(string).replace(/[&<>"'\/]/g, function (s) {
          return entityMap[s];
        });
    }

    // ================== delete lines ================== 
    $("#deletelines").click(function(){ 
        if ($('#datatables >tbody >tr').length > 1) {
            $('#datatables tbody tr').each(function(){   
                if ($(this).find('input.lineitem:checked').length >= 1 && $('#datatables >tbody >tr').length > 1) {
                    $(this).remove();
                }
            });
            
            //re-assign row number
            $('#datatables tbody tr').each(function(){
                var iRow = $(this).index();
                $(this).find('td:eq(1)').html(iRow+1);
                $(this).find('#id_buku').attr("name", 'order['+iRow+'][id_buku]');
                $(this).find('#judul_buku').attr("name", 'order['+iRow+'][judul_buku]');
                $(this).find('#id_peminjaman').attr("name", 'order['+iRow+'][id_peminjaman]');
                $(this).find('#id_peminjaman_detail').attr("name", 'order['+iRow+'][id_peminjaman_detail]');
                $(this).find('#base_line').attr("name", 'order['+iRow+'][base_line]');
            });
        } 
        
        if ($('#datatables >tbody >tr').length == 1) {
            var iRow = 0; 
            $('#datatables tbody tr').each(function(){   
                if ($(this).find('input.lineitem:checked').length >= 1 && $('#datatables >tbody >tr').length == 1) { 
                    $(this).find('td:eq(1)').html(iRow+1);
                    $(this).find('tbody tr td:eq(2)').html('');
                    $(this).find('tbody tr td:eq(3)').html('');
                    $(this).find('tbody tr td:eq(4)').html('');
                    $(this).find('tbody tr td:eq(5)').html('');
                    $(this).find('#id_buku').attr("name", 'order['+iRow+'][id_buku]');
                    $(this).find('#judul_buku').attr("name", 'order['+iRow+'][judul_buku]');
                    $(this).find('#id_peminjaman').attr("name", 'order['+iRow+'][id_peminjaman]');
                    $(this).find('#id_peminjaman_detail').attr("name", 'order['+iRow+'][id_peminjaman_detail]');
                    $(this).find('#base_line').attr("name", 'order['+iRow+'][base_line]');
                    $(this).find('tbody tr #id_buku').val('');
                    $(this).find('tbody tr #judul_buku').val('');
                    $(this).find('tbody tr #id_peminjaman').val('');
                    $(this).find('tbody tr #id_peminjaman_detail').val('');
                    $(this).find('tbody tr #base_line').val('');
                }
            }); 
        }

    });

    $('#initTable').click(function(){
        initializeTable($('#datatables'));
    });

    function initializeTable(jQtable) {
        jQtable.find("tr:gt(1)").remove();
        jQtable.find('tbody tr td:eq(1)').html(1);
        jQtable.find('tbody tr td:eq(2)').html('');
        jQtable.find('tbody tr td:eq(3)').html('');
        jQtable.find('tbody tr td:eq(4)').html('');
        jQtable.find('tbody tr td:eq(5)').html('');
        jQtable.find('tbody tr #id_buku').attr("name", 'order[0][id_buku]');
        jQtable.find('tbody tr #judul_buku').attr("name", 'order[0][judul_buku]');
        jQtable.find('tbody tr #id_peminjaman').attr("name", 'order[0][id_peminjaman]');
        jQtable.find('tbody tr #id_peminjaman_detail').attr("name", 'order[0][id_peminjaman_detail]');
        jQtable.find('tbody tr #base_line').attr("name", 'order[0][base_line]');
        jQtable.find('tbody tr #id_buku').val('');
        jQtable.find('tbody tr #judul_buku').val('');
        jQtable.find('tbody tr #id_peminjaman').val('');
        jQtable.find('tbody tr #id_peminjaman_detail').val('');
        jQtable.find('tbody tr #base_line').val('');
        calcTotal();
    }

    $('#datatables .group-checkable').change(function () {
        var set = jQuery(this).attr("data-set");
        var checked = jQuery(this).is(":checked");
        jQuery(set).each(function () {
            if (checked) {
                $(this).attr("checked", true);
                $(this).parents('tr').addClass("active");
            } else {
                $(this).attr("checked", false);
                $(this).parents('tr').removeClass("active");
            }               
        });
        jQuery.uniform.update(set);

    });

    // ============================ ADD NEW ROW IN DATATABLES EVENT ============================
    function addLines(){
        $("#datatables tr.tblRowSO:last").each(function() { 
            if ($(this).find('td:eq(2)').html() != '') { 
                var i = $('#datatables tr.tblRowSO').length;
                var sName;
                $("#datatables tr.tblRowSO:last").clone().find("input").each(function() {
                    if ($(this).prop('name').indexOf('id_buku') > -1) sName = 'order['+i+'][id_buku]';
                    if ($(this).prop('name').indexOf('judul_buku') > -1) sName = 'order['+i+'][judul_buku]';
                    if ($(this).prop('name').indexOf('id_peminjaman') > -1) sName = 'order['+i+'][id_peminjaman]';
                    if ($(this).prop('name').indexOf('id_peminjaman_detail') > -1) sName = 'order['+i+'][id_peminjaman_detail]';
                    if ($(this).prop('name').indexOf('base_line') > -1) sName = 'order['+i+'][base_line]';
                    
                    $(this).prop({
                      'name': function(_, name) { return sName },
                      'value': ''               
                    });
                }).end().appendTo("#datatables");
                 
                $("#datatables tr:last").each(function() {
                    $(this).removeClass("danger");
                });
                
                var x=1;
                $("#datatables tr:last td").each(function() {
                    var iRow = $(this).closest("tr").prevAll("tr").length;
                    if (x!=1 && x!=2 && x!=3 && x!=4 && x!=5) $(this).html('');
                    if (x==2) $(this).html(iRow+1);
                    x++;
                });
                $("input:checkbox").uniform(); 
                $.uniform.update();
            }
        }); 
    } 
    // ============================ ADD NEW ROW IN DATATABLES EVENT ============================

    $('#findDataBtn').click(function(){

        var el = $(".ItemData");
        var dataBuku = $('#id_databuku').val().trim();
        var url = '/perpus/Buku/searchBuku/' +urlencode(dataBuku);
        var matched = false;

        if (dataBuku == "")
        {
            alert('Harap pilih Buku terlebih dahulu');
            $('#id_databuku').focus();
            return false;
        }
        else if (dataBuku != "")
        {
            $('#datatables tbody tr.tblRowSO').each(function(){
                idbuku = $(this).closest('tr').find('#id_buku').val(); 
                if (dataBuku == idbuku) {
                  alert('Buku Sudah Ada Dalam Daftar');
                  matched = true; 
                }
            });

            if (!matched) {
                App.blockUI(el);

                $.getJSON({
                    type: "GET",
                    url: url,
                    data: {
                        dataBuku: dataBuku
                    },
                    success: function (data) {

                        window.setTimeout(function () {
                            App.unblockUI(el);
                        }, 100);

                        var len = data.length; 
                        if (len >= 1) { 
                            
                            addLines();
                            $('#datatables tbody tr:last').each(function(){   
                                $(this).find('td:eq(2)').html(data[0].judul_buku);
                                $(this).find('td:eq(3)').html(data[0].kategori);
                                $(this).find('td:eq(4)').html(data[0].penerbit);
                                $(this).find('td:eq(5)').html(data[0].tahun_cetak);
                                $(this).find('#id_buku').prop("value", data[0].id_buku);
                                $(this).find('#judul_buku').prop("value", data[0].judul_buku);
                            });
                            $('#id_databuku').val('');

                        } else  {
                            alert('Data Tidak Ditemukan'); 
                        }
                    },
                    error: function () {
                        alert("Application error. Please try again.");
                    }
                });
            }

        }
        return false;

    });


}(jQuery));