-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 20 Mei 2019 pada 05.58
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpus`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` int(3) NOT NULL,
  `kode_anggota` varchar(10) NOT NULL,
  `nama_anggota` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `pob` varchar(50) NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `kode_anggota`, `nama_anggota`, `dob`, `pob`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, '1234567890', 'IRVAN AGUSSUSANTO', '1989-08-27', 'JAKARTA', 'Y', 1, '2019-05-12 14:15:26', 1, '2019-05-12 14:16:41'),
(2, '0987654321', 'INDRA', '2019-05-12', 'JAKARTA', 'Y', 1, '2019-05-12 14:17:38', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `id_buku` int(3) NOT NULL,
  `judul_buku` varchar(50) NOT NULL,
  `deskripsi_buku` varchar(255) NOT NULL,
  `id_kategori` int(3) NOT NULL,
  `id_penerbit` int(3) NOT NULL,
  `tahun_cetak` varchar(4) NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`id_buku`, `judul_buku`, `deskripsi_buku`, `id_kategori`, `id_penerbit`, `tahun_cetak`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(3, 'PHP Code', 'Mempelajari struktur dasar pemograman', 5, 6, '2019', 'Y', 1, '2019-05-12 16:44:10', 0, '0000-00-00 00:00:00'),
(4, 'C# Code', 'Mempelajari struktur dasar pemograman C#', 5, 6, '2019', 'Y', 1, '2019-05-12 16:44:49', 0, '0000-00-00 00:00:00'),
(5, 'HTML CSS', 'Dasar Programming', 5, 6, '2019', 'Y', 1, '2019-05-18 21:25:12', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(3) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(5, 'Computer', 'Y', 1, '2019-05-11 17:04:07', 0, '0000-00-00 00:00:00'),
(6, 'Olahraga', 'Y', 1, '2019-05-11 17:04:27', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman_detail`
--

CREATE TABLE `peminjaman_detail` (
  `id_peminjaman_detail` int(3) NOT NULL,
  `id_peminjaman` int(3) NOT NULL,
  `id_buku` int(3) NOT NULL,
  `judul_buku` varchar(50) NOT NULL,
  `line_num` int(3) NOT NULL,
  `line_status` char(1) NOT NULL DEFAULT 'O',
  `review` char(1) NOT NULL DEFAULT 'N',
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman_detail`
--

INSERT INTO `peminjaman_detail` (`id_peminjaman_detail`, `id_peminjaman`, `id_buku`, `judul_buku`, `line_num`, `line_status`, `review`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(4, 2, 3, 'PHP Code', 0, 'C', 'N', 1, '2019-05-18 16:22:11', 1, '2019-05-18 21:16:15'),
(6, 1, 3, 'PHP Code', 0, 'O', 'N', 1, '2019-05-18 16:44:56', 1, '2019-05-20 10:45:42'),
(7, 2, 4, 'C# Code', 1, 'C', 'N', 1, '2019-05-18 20:48:19', 1, '2019-05-18 21:09:14'),
(8, 3, 4, 'C# Code', 0, 'C', 'N', 1, '2019-05-18 21:25:54', 1, '2019-05-19 15:11:29'),
(9, 3, 5, 'HTML CSS', 1, 'C', 'N', 1, '2019-05-18 21:25:54', 1, '2019-05-19 15:08:57'),
(10, 3, 3, 'PHP Code', 2, 'C', 'N', 1, '2019-05-18 21:25:54', 1, '2019-05-19 14:55:18'),
(11, 4, 4, 'C# Code', 0, 'C', 'Y', 1, '2019-05-20 10:38:50', 2, '2019-05-20 10:56:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman_header`
--

CREATE TABLE `peminjaman_header` (
  `id_peminjaman` int(3) NOT NULL,
  `peminjaman_number` varchar(20) NOT NULL,
  `id_anggota` int(3) NOT NULL,
  `status` char(1) NOT NULL,
  `tanggal_peminjaman` date NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman_header`
--

INSERT INTO `peminjaman_header` (`id_peminjaman`, `peminjaman_number`, `id_anggota`, `status`, `tanggal_peminjaman`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'A20190517001', 1, 'O', '2019-05-18', 1, '2019-05-18 16:21:42', 1, '2019-05-18 16:44:56'),
(2, 'A20190517002', 1, 'C', '2019-05-18', 1, '2019-05-18 16:22:11', 1, '2019-05-18 20:48:39'),
(3, 'A20190517003', 1, 'C', '2019-05-18', 1, '2019-05-18 21:25:54', 1, '2019-05-18 21:31:23'),
(4, '1212121', 2, 'C', '2019-05-20', 1, '2019-05-20 10:38:50', 1, '2019-05-20 10:39:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penerbit`
--

CREATE TABLE `penerbit` (
  `id_penerbit` int(3) NOT NULL,
  `penerbit` varchar(100) NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penerbit`
--

INSERT INTO `penerbit` (`id_penerbit`, `penerbit`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(5, 'Elexmedia Komputindo', 'Y', 1, '2019-05-11 17:03:29', 0, '0000-00-00 00:00:00'),
(6, 'Gramedia', 'Y', 1, '2019-05-11 17:03:43', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengembalian_detail`
--

CREATE TABLE `pengembalian_detail` (
  `id_pengembalian_detail` int(3) NOT NULL,
  `id_pengembalian` int(3) NOT NULL,
  `id_peminjaman` int(3) NOT NULL,
  `id_buku` int(3) NOT NULL,
  `judul_buku` varchar(50) NOT NULL,
  `base_line` int(3) NOT NULL,
  `line_num` int(3) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengembalian_detail`
--

INSERT INTO `pengembalian_detail` (`id_pengembalian_detail`, `id_pengembalian`, `id_peminjaman`, `id_buku`, `judul_buku`, `base_line`, `line_num`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(3, 3, 2, 3, 'PHP Code', 0, 0, 1, '2019-05-18 21:16:15', 0, '0000-00-00 00:00:00'),
(4, 4, 3, 5, 'HTML CSS', 1, 0, 1, '2019-05-18 21:27:10', 0, '0000-00-00 00:00:00'),
(5, 5, 3, 4, 'C# Code', 0, 0, 1, '2019-05-18 21:29:23', 0, '0000-00-00 00:00:00'),
(7, 2, 2, 4, 'C# Code', 0, 0, 1, '2019-05-20 09:52:39', 0, '0000-00-00 00:00:00'),
(8, 7, 4, 4, 'C# Code', 0, 0, 1, '2019-05-20 10:39:28', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengembalian_header`
--

CREATE TABLE `pengembalian_header` (
  `id_pengembalian` int(3) NOT NULL,
  `id_peminjaman` int(3) NOT NULL,
  `pengembalian_number` varchar(20) NOT NULL,
  `id_anggota` int(3) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'D',
  `tanggal_pengembalian` date NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengembalian_header`
--

INSERT INTO `pengembalian_header` (`id_pengembalian`, `id_peminjaman`, `pengembalian_number`, `id_anggota`, `status`, `tanggal_pengembalian`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(2, 0, 'B20190518001', 1, 'O', '2019-05-18', 1, '2019-05-18 21:09:14', 1, '2019-05-20 09:52:39'),
(3, 0, 'B20190518002', 1, 'O', '2019-05-18', 1, '2019-05-18 21:16:15', 0, '0000-00-00 00:00:00'),
(4, 3, 'B20190518003', 1, 'O', '2019-05-18', 1, '2019-05-18 21:27:10', 0, '0000-00-00 00:00:00'),
(5, 3, 'B20190518004', 1, 'O', '2019-05-18', 1, '2019-05-18 21:29:23', 0, '0000-00-00 00:00:00'),
(6, 3, 'B20190518005', 1, 'O', '2019-05-18', 1, '2019-05-18 21:31:23', 0, '0000-00-00 00:00:00'),
(7, 4, '121211111', 2, 'O', '2019-05-20', 1, '2019-05-20 10:39:28', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rating`
--

CREATE TABLE `rating` (
  `id_rating` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `rating` float NOT NULL,
  `created_date` datetime NOT NULL,
  `created_user` int(3) NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review`
--

CREATE TABLE `review` (
  `id_review` int(3) NOT NULL,
  `id_peminjaman_detail` int(3) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `id_buku` int(3) NOT NULL,
  `judul_buku` varchar(50) NOT NULL,
  `book_rate` char(1) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `review`
--

INSERT INTO `review` (`id_review`, `id_peminjaman_detail`, `id_anggota`, `id_buku`, `judul_buku`, `book_rate`, `remarks`, `created_user`, `created_date`) VALUES
(1, 11, 2, 4, 'C# Code', '5', 'OKE', 2, '2019-05-20 10:56:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `user_id` int(3) NOT NULL,
  `username` varchar(30) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `id_usergroup` int(3) NOT NULL,
  `aktif` char(1) DEFAULT 'Y',
  `created_user` int(3) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_user` int(3) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `username`, `fullname`, `password`, `id_usergroup`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'admin', 'Administrator', '21232f297a57a5a743894a0e4a801fc3', 1, 'Y', 1, '2018-06-14 00:00:00', NULL, NULL),
(2, 'manager', 'Manager', '81dc9bdb52d04dc20036dbd8313ed055', 2, 'Y', 1, '2018-12-27 22:11:46', NULL, NULL),
(3, 'penjualan', 'Penjualan', '81dc9bdb52d04dc20036dbd8313ed055', 3, 'Y', 1, '2018-12-27 22:13:09', NULL, NULL),
(4, 'gudang', 'Gudang', '81dc9bdb52d04dc20036dbd8313ed055', 4, 'Y', 1, '2018-12-27 22:13:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `usergroup`
--

CREATE TABLE `usergroup` (
  `id_usergroup` int(3) NOT NULL,
  `usergroup` varchar(30) NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usergroup`
--

INSERT INTO `usergroup` (`id_usergroup`, `usergroup`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'Administrator', 'Y', 1, '2018-06-14 00:00:00', 0, '0000-00-00 00:00:00'),
(2, 'Manager', 'Y', 1, '2018-11-20 17:35:17', 0, '0000-00-00 00:00:00'),
(3, 'Penjualan', 'Y', 1, '2018-11-20 17:35:47', 0, '0000-00-00 00:00:00'),
(4, 'Gudang', 'Y', 1, '2018-11-20 17:35:58', 0, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `peminjaman_detail`
--
ALTER TABLE `peminjaman_detail`
  ADD PRIMARY KEY (`id_peminjaman_detail`);

--
-- Indexes for table `peminjaman_header`
--
ALTER TABLE `peminjaman_header`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `penerbit`
--
ALTER TABLE `penerbit`
  ADD PRIMARY KEY (`id_penerbit`);

--
-- Indexes for table `pengembalian_detail`
--
ALTER TABLE `pengembalian_detail`
  ADD PRIMARY KEY (`id_pengembalian_detail`);

--
-- Indexes for table `pengembalian_header`
--
ALTER TABLE `pengembalian_header`
  ADD PRIMARY KEY (`id_pengembalian`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id_rating`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id_review`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `usergroup`
--
ALTER TABLE `usergroup`
  ADD PRIMARY KEY (`id_usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id_anggota` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `peminjaman_detail`
--
ALTER TABLE `peminjaman_detail`
  MODIFY `id_peminjaman_detail` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `peminjaman_header`
--
ALTER TABLE `peminjaman_header`
  MODIFY `id_peminjaman` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `penerbit`
--
ALTER TABLE `penerbit`
  MODIFY `id_penerbit` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pengembalian_detail`
--
ALTER TABLE `pengembalian_detail`
  MODIFY `id_pengembalian_detail` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pengembalian_header`
--
ALTER TABLE `pengembalian_header`
  MODIFY `id_pengembalian` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id_review` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `usergroup`
--
ALTER TABLE `usergroup`
  MODIFY `id_usergroup` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
