		<?php  
		if($this->session->flashdata('error')){
			$error = $this->session->flashdata('message') ? $this->session->flashdata('message') : $this->session->flashdata('error');
			echo "<div class='alert alert-danger'>
				<a class='close' data-dismiss='alert'>×</a>
				". $error ."</div>";
		}elseif($this->session->flashdata('success') || $this->session->flashdata('message')){
			$message = $this->session->flashdata('message') ? $this->session->flashdata('message') : $this->session->flashdata('success');
			echo "<div class='alert alert-success alert-login'>
				<a class='close' data-dismiss='alert'>×</a>
				" . $message . "</div>";
		}?>
    