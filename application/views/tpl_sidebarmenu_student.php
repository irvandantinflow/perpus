<div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('Home')?>" class="site_title"> <span>Perpustakaan</span></a>
            </div>

            <div class="clearfix"></div>
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                      <li class="<?php echo ($active == 'Home') ? 'active' : ''; ?>"><a href="<?php echo base_url('Student'); ?>"><i class="fa fa-user"></i> Home </a></li>
                      <li class="<?php echo ($active == 'Profil') ? 'active' : ''; ?>"><a><i class="fa fa-book"></i> Profil </a></li>
                      <li class="<?php echo ($active == 'Pencarian') ? 'active' : ''; ?>"><a href="<?php echo base_url('student/Pencarian'); ?>">Pencarian</a></li>
                      <li class="<?php echo ($active == 'Review') ? 'active' : ''; ?>"><a href="<?php echo base_url('student/Review'); ?>">Review</a></li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $this->session->userdata('nama_anggota') ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url('Student/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->