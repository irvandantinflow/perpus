<div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('Home')?>" class="site_title"> <span>Perpustakaan</span></a>
            </div>

            <div class="clearfix"></div>
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                    <?php if($this->session->userdata('level') == '1') { ?>
                      <li class="<?php echo ($active == 'Anggota') ? 'active' : ''; ?>"><a><i class="fa fa-user"></i> Anggota <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="<?php echo ($active == 'Anggota') ? 'display:block' : ''; ?>">
                          <li class="<?php echo activate_menu('Anggota'); ?>"><a href="<?php echo base_url('Anggota'); ?>">Anggota</a></li>
                        </ul>
                      </li>
                      <li class="<?php echo ($active == 'Transaction') ? 'active' : ''; ?>"><a><i class="fa fa-book"></i> Transaction <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="<?php echo ($active == 'Transaction') ? 'display:block' : ''; ?>">
                          <li class="<?php echo activate_menu('Peminjaman'); ?>"><a href="<?php echo base_url('Peminjaman'); ?>">Peminjaman</a></li>
                          <li class="<?php echo activate_menu('Pengembalian'); ?>"><a href="<?php echo base_url('Pengembalian'); ?>">Pengembalian</a></li>
                          <li class="<?php echo activate_menu('Review'); ?>"><a href="<?php echo base_url('Review'); ?>">Review</a></li>
                        </ul>
                      </li>
                      <li class="<?php echo ($active == 'Master') ? 'active' : ''; ?>"><a><i class="fa fa-cogs"></i> Master <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="<?php echo ($active == 'Master') ? 'display:block' : ''; ?>">
                          <li class="<?php echo activate_menu('User'); ?>"><a href="<?php echo base_url('User'); ?>">User</a></li>
                          <li class="<?php echo activate_menu('Usergroup'); ?>"><a href="<?php echo base_url('Usergroup'); ?>">Usergroup</a></li>
                          <li class="<?php echo activate_menu('Kategori'); ?>"><a href="<?php echo base_url('Kategori'); ?>">Kategori</a></li>
                          <li class="<?php echo activate_menu('Penerbit'); ?>"><a href="<?php echo base_url('Penerbit'); ?>">Penerbit</a></li>
                          <li class="<?php echo activate_menu('Buku'); ?>"><a href="<?php echo base_url('Buku'); ?>">Buku</a></li>
                        </ul>
                      </li>
                    <?php } ?>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $this->session->userdata('fullname') ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url('Inadminpage/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->