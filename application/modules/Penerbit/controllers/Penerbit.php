<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerbit extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Penerbit_model');
		if(!is_logged_in()){redirect('Inadminpage');}
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }	
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Master',
	   		'content'	=> 'Penerbit/list_penerbit',
	   		'title'		=> 'Penerbit',
	   		'list'		=> $this->Penerbit_model->getAllList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Penerbit/form_penerbit',
		   	'title'			=> 'New Penerbit',
	   		'page_action' 	=> base_url('Penerbit/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){
		$this->form_validation->set_rules('penerbit','Kode Penerbit', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Penerbit');

		} else {

			$data = array(
				'penerbit'			=> $accept['penerbit'],
				'aktif'				=> isset($accept['aktif']) ? $accept['aktif'] : 'N'
			);

			$query = $this->Penerbit_model->save($accept['id_penerbit'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Penerbit');
		}	
	}

	public function delete($id){
		if (!$id) redirect('Penerbit');	
		$query = $this->Penerbit_model->delete($id);
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		else
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		
		redirect('Penerbit');
	}

	function edit($id = 0){	
		if(!$this->Penerbit_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Penerbit');
		};

	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Penerbit/form_penerbit',
	   		'title'			=> 'Edit Penerbit',
	   		'page_action' 	=> base_url('Penerbit/save'),
	   		'get_data'		=> $this->Penerbit_model->edit($id),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}