<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penerbit_model extends CI_Model 
{    
	var $table = 'penerbit';
	var $primary = 'id_penerbit';

    public function __construct(){
        parent::__construct(); 
    }

    public function getAllList(){
        $query = $this->db->get($this->table);
        return $query;
    }

    public function save($id ='', $data =''){

    	if(!empty($data)){
    		$this->db->where($this->primary, $id);
			$is_check = $this->db->get($this->table);

			if($is_check->num_rows() > 0){

				$this->db->set($data);
				$this->db->set('updated_user', $this->session->userdata('user_id'));
				$this->db->set('updated_date', 'now()', FALSE);
				$this->db->where($this->primary, $id);
				
				$query = $this->db->update($this->table);
				if($query)
					return true;
				else
					return false;
			} else {
				$this->db->set('created_user', $this->session->userdata('user_id'));
				$this->db->set('created_date', 'now()', FALSE);
				$query = $this->db->insert($this->table, $data);
				if($query)
					return true;
				else
					return false;
			}
    	}  	
    }

    public function delete($id){
		$query = $this->db->delete($this->table,array($this->primary=>$id));
		if($query)
			return true;
		else
			return false;
	}

	function checkID($id){
		$query = $this->db->get_where($this->table, array($this->primary=>$id));
		if ($query->num_rows() > 0) 
			return true;
		else
			return false;
    }

    public function edit($id){
		$query = $this->db->get_where($this->table, array($this->primary=>$id));
		return $query;
	}

	function getPenerbitAktif(){
        $query = $this->db->query("SELECT id_penerbit, penerbit from penerbit where aktif = 'Y' order by penerbit asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function getPenerbitAll(){
        $query = $this->db->query("SELECT id_penerbit, penerbit from penerbit order by penerbit asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

}   
