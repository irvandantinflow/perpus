<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Kategori_model');
		if(!is_logged_in()){redirect('Inadminpage');}
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }	
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Master',
	   		'content'	=> 'Kategori/list_kategori',
	   		'title'		=> 'Kategori',
	   		'list'		=> $this->Kategori_model->getAllList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Kategori/form_kategori',
		   	'title'			=> 'New Kategori',
	   		'page_action' 	=> base_url('Kategori/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){
		$this->form_validation->set_rules('kategori','Kode Kategori', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Kategori');

		} else {

			$data = array(
				'kategori'			=> $accept['kategori'],
				'aktif'				=> isset($accept['aktif']) ? $accept['aktif'] : 'N'
			);

			$query = $this->Kategori_model->save($accept['id_kategori'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Kategori');
		}	
	}

	public function delete($id){
		if (!$id) redirect('Kategori');	
		$query = $this->Kategori_model->delete($id);
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		else
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		
		redirect('Kategori');
	}

	function edit($id = 0){	
		if(!$this->Kategori_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Kategori');
		};

	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Kategori/form_kategori',
	   		'title'			=> 'Edit Kategori',
	   		'page_action' 	=> base_url('Kategori/save'),
	   		'get_data'		=> $this->Kategori_model->edit($id),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}