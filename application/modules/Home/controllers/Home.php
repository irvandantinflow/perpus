<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Home_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
	}

	public function index(){  
		
	   	$data = array(
	   		'active'			=> 'Rating',
	   		'content'			=> 'Home/home',
	   		'dtAnggota'		=> $this->Home_model->getAnggota(),
	   		'dtBuku'		=> $this->Home_model->getBuku(),
	   		'dtPenerbit'		=> $this->Home_model->getPenerbit(),
	   		'dtKategori'		=> $this->Home_model->getKategori()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}