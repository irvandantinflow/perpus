<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inadminpage_model extends CI_Model 
{
    public function __construct(){
        parent::__construct(); 
    }
    
    function restrict($logged_out = FALSE){

		// If the user is logged in and he's trying to access a page
		// he's not allowed to see when logged in,
		// redirect him to the index!
		if ($logged_out && is_logged_in()){
	      	echo $this->fungsi->warning('Maaf, sepertinya Anda sudah login...',site_url());
	      	die();
		}
		
		// If the user isn' logged in and he's trying to access a page
		// he's not allowed to see when logged out,
		// redirect him to the login page!
		if ( ! $logged_out && !is_logged_in()){
		    echo $this->fungsi->warning('Anda diharuskan untuk Login bila ingin mengakses halaman ini.',site_url());
		    die();
		}
	}
	
	function do_login($login = NULL){

     	// A few safety checks
    	// Our array has to be set
    	if(!isset($login))
	        return FALSE;
		
    	//Our array has to have 2 values
     	//No more, no less!
    	if(count($login) != 2)
		return FALSE;
	
     	$username = $login['username'];
     	$password = md5($login['password']);

     	$sql    = "SELECT A.user_id, A.username, A.fullname, A.id_usergroup,
     				C.usergroup
                    FROM user A
                        LEFT JOIN usergroup C on A.id_usergroup = C.id_usergroup 
                    WHERE A.aktif = 'Y' AND A.username = '".$username."' AND A.password = '".$password."'";

        $query=$this->db->query($sql);

     	foreach ($query->result() as $row){
    		$user_id        	= $row->user_id;
			$username       	= $row->username;
			$fullname       	= $row->fullname;
			$level          	= $row->id_usergroup;
			$usergroup          = $row->usergroup;
    	}

     	if ($query->num_rows() == 1){
       	 	$newdata = array(
       	 	    'user_id'		=> $user_id,
              	'username'  	=> $username,
                'fullname'  	=> $fullname,
                'logged_in' 	=> TRUE,
		    	'level'			=> $level,
		    	'usergroup'		=> $usergroup,
		    	'kode_anggota'	=> ''
           		);
			// Our user exists, set session.
			$this->session->set_userdata($newdata);	  
			
			return TRUE;
		} else {
			// No existing user.
			return FALSE;
		}
	}
}