<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjaman extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Peminjaman_model');
		$this->load->model('Anggota/Anggota_model');
		$this->load->model('Buku/Buku_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Transaction',
	   		'content'	=> 'Peminjaman/list_peminjaman',
	   		'title'		=> 'Peminjaman',
	   		'list'		=> $this->Peminjaman_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Transaction',
	   		'content'		=> 'Peminjaman/form_peminjaman',
		   	'title'			=> 'New Peminjaman',
		   	'anggota'		=> 	$this->Anggota_model->getAnggotaAktif(),
		   	'buku'			=> 	$this->Buku_model->getBukuAktif(),
	   		'page_action' 	=> base_url('Peminjaman/add_process'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function add_process(){
		
		$data 	= $this->fungsi->accept_data(array_keys($_POST));

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'			=> $i,
				'line_status'		=> 'O',
				'id_buku'			=> $row['id_buku'],
				'judul_buku'		=> $row['judul_buku']
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'peminjaman_number'			=> $data['peminjaman_number'],
			'id_anggota'				=> $data['id_anggota'],
			'status'					=> 'D',
			'tanggal_peminjaman'		=> isset($data['tanggal_peminjaman']) ? date('Y-m-d', strtotime($data['tanggal_peminjaman'])) : '',
		);

		$query = $this->Peminjaman_model->addData($data_header, $data_detail);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Peminjaman');

	}

	function edit($id = 0){	
		if(!$this->Peminjaman_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Peminjaman');
		};

	   	$data = array(
	   		'active'			=> 'Transaction',
	   		'content'			=> 'Peminjaman/form_peminjaman',
	   		'title'				=> 'Edit Peminjaman',
	   		'page_action' 		=> base_url('Peminjaman/update_process'),
	   		'anggota'			=> 	$this->Anggota_model->getAnggotaAktif(),
		   	'buku'				=> 	$this->Buku_model->getBukuAktif(),
	   		'get_data'			=> $this->Peminjaman_model->edit($id),
	   		'get_data_detail'	=> $this->Peminjaman_model->editdetail($id)
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function update_process(){
		
		$data 			= $this->fungsi->accept_data(array_keys($_POST));
		$idPinjam		= $data['id_peminjaman'];

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'				=> $i,
				'id_buku'				=> $row['id_buku'],
				'judul_buku'			=> $row['judul_buku'],
				'id_peminjaman_detail'	=> isset($row['id_peminjaman_detail']) ? $row['id_peminjaman_detail'] : 0,	
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'peminjaman_number'		=> $data['peminjaman_number'],
			'id_anggota'			=> $data['id_anggota'],
			'status'				=> $data['status'],
			'tanggal_peminjaman'	=> isset($data['tanggal_peminjaman']) ? date('Y-m-d', strtotime($data['tanggal_peminjaman'])) : '',	
		);

		$query = $this->Peminjaman_model->updateData($data_header, $data_detail, $idPinjam);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Peminjaman');

	}
}