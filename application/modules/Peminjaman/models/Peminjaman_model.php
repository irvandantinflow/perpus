<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Peminjaman_model extends CI_Model 
{    
	var $table = 'peminjaman_header';
	var $primary = 'id_peminjaman';

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT T0.*,
        							T1.kode_anggota, T1.nama_anggota,
        							CASE T0.status WHEN 'D' THEN 'DRAFT' WHEN 'O' THEN 'OPEN' ELSE 'CLOSE' END AS statuspeminjaman 
        							FROM peminjaman_header T0
									LEFT JOIN anggota T1 ON T0.id_anggota = T1.id_anggota
        							order by T0.peminjaman_number DESC");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function addData($data_header = '', $data_detail = ''){
    
        $this->db->trans_start();
        
        // Insert Into header Start
        $this->db->set('created_user', $this->session->userdata('user_id'));
        $this->db->set('created_date', 'now()', FALSE);
        $this->db->set($data_header);
        
        $query = $this->db->insert('peminjaman_header');
        if(!$query) return false;
        $ID = $this->db->insert_id();
        // Insert Into header End
        
        // Insert Into detail Start
        for($i = 0; $i < count($data_detail); $i++){
            $this->db->set('id_peminjaman', $ID, FALSE);
            $this->db->set('created_user', $this->session->userdata('user_id'));
            $this->db->set('created_date', 'now()', FALSE);
            $this->db->set($data_detail[$i]);
            
            $query = $this->db->insert('peminjaman_detail');
            if(!$query) return false;
        }
        // Insert Into detail End
        
        $this->db->trans_complete();
        
        return true;
    }

	function checkID($id){
		$query = $this->db->get_where($this->table, array($this->primary=>$id));
		if ($query->num_rows() > 0) 
			return true;
		else
			return false;
    }

    function checkBaseStatus($id){
        $query = $this->db->query("SELECT * FROM ".$this->table." WHERE ".$this->primary." = '".$id."' AND status = 'C'");
        if($query->num_rows() > 0)
            return FALSE;
        else
            return true;
    }

    public function edit($id){
        $query = $this->db->query("SELECT *, CASE WHEN status = 'D' THEN 'DRAFT' WHEN status = 'O' THEN 'OPEN' WHEN status = 'C' THEN 'CLOSE' ELSE '' END AS 'statuspeminjaman' FROM peminjaman_header WHERE id_peminjaman = '".$id."'");
        return $query;
    }

    public function editdetail($id){
        $query = $this->db->query("SELECT A.*,
        							B.tahun_cetak,
        							C.kategori,
        							D.penerbit
                                    FROM peminjaman_detail A
                                    LEFT JOIN buku B ON A.id_buku = B.id_buku
                                    LEFT JOIN kategori C ON B.id_kategori = C.id_kategori
                                    LEFT JOIN penerbit D ON B.id_penerbit = D.id_penerbit
                                    WHERE id_peminjaman = '".$id."'
                                    order by line_num ");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function editDetailOpen($id){
        $query = $this->db->query("SELECT A.*,
                                    B.tahun_cetak,
                                    C.kategori,
                                    D.penerbit
                                    FROM peminjaman_detail A
                                    LEFT JOIN buku B ON A.id_buku = B.id_buku
                                    LEFT JOIN kategori C ON B.id_kategori = C.id_kategori
                                    LEFT JOIN penerbit D ON B.id_penerbit = D.id_penerbit
                                    WHERE id_peminjaman = '".$id."'
                                    AND A.line_status = 'O'
                                    order by line_num ");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function updateData($data_header = '', $data_detail = '', $idpinjam = 0){

        $this->db->trans_start();

        // Update Into header Start
        $this->db->set('updated_user', $this->session->userdata('user_id'), FALSE);
        $this->db->set('updated_date', 'now()', FALSE);
        $this->db->set($data_header); 
        $this->db->where('id_peminjaman', $idpinjam); 
        $query = $this->db->update('peminjaman_header');
        if(!$query) return false;
        // Update Into header End

        // Update Into detail Start
        $itd    = '';
        foreach($data_detail as $row) {
            $itDetailID = $row['id_peminjaman_detail'];
            unset($row['id_peminjaman_detail']);
            
            if ($itDetailID > 0) { 
                
                $itd    .= ($itd == '') ? $itDetailID : ','.$itDetailID; 
                $this->db->set('id_peminjaman', $idpinjam, FALSE);
                $this->db->set('updated_user', $this->session->userdata('user_id'));
                $this->db->set('updated_date', 'now()', FALSE);
                $this->db->set($row);
                $this->db->where('id_peminjaman_detail', $itDetailID); 
                $query = $this->db->update('peminjaman_detail'); 

            }else { 
                
                $this->db->set('id_peminjaman', $idpinjam, FALSE);
                $this->db->set('created_user', $this->session->userdata('user_id'));
                $this->db->set('created_date', 'now()', FALSE);
                $this->db->set($row);
                $query = $this->db->insert('peminjaman_detail'); 
                $ID = $this->db->insert_id();
                $itd    .= ($itd == '') ? $ID : ','.$ID; 

            } 
        }
        // Update Into detail End

        if ($itd != '') {
            $this->db->query("DELETE FROM peminjaman_detail where id_peminjaman = ? and id_peminjaman_detail not in (".$itd.")", $idpinjam);
        }

        $this->db->trans_complete(); 

        return true;
    }

}   
