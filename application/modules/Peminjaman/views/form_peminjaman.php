<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left"></div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $title; ?> </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>

          <form class="form-horizontal form-label-left" method="POST" action="<?php echo $page_action;?>" >
          <!-- HEADER SIDE -->
          <div class="x_content">
            <br />

              <!-- LEFT COLOUM -->
              <div class="col-md-6">

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Anggota</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <select class="form-control select2_single" name="id_anggota" >
                      <option value="">-- Select Anggota --</option>
                      <?php foreach ($anggota->result() as $baris) {
                        if (isset($get_data) && $baris->id_anggota == $get_data->row()->id_anggota) { ?>
                        <option value="<?php echo $baris->id_anggota; ?>" selected><?php echo strtoupper($baris->kode_anggota).' - '.strtoupper($baris->nama_anggota); ?></option>
                         <?php } else { ?>
                        <option value="<?php echo $baris->id_anggota; ?>"><?php echo strtoupper($baris->kode_anggota).' - '.strtoupper($baris->nama_anggota); ?></option>
                      <?php } }?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php if (isset($get_data) && $get_data->row()->status == 'D'){?>
                      <select class="form-control select2_single" style="width: 100%;" name="status">
                        <option value="D" <?php if(isset($get_data) && $get_data->row()->status == "D") echo 'selected'; ?>>Draft</option>
                        <option value="O" <?php if(isset($get_data) && $get_data->row()->status == "O") echo 'selected'; ?>>Open</option>
                      </select>
                    <?php } else { ?>
                      <input type="hidden" name="status" value="<?php echo isset($get_data) ? $get_data->row()->status : ''; ?>">
                      <input type="text" class="form-control" name="statuspurchase" readonly="" value="<?php echo isset($get_data) ? $get_data->row()->statuspeminjaman : 'Draft'; ?>">
                    <?php } ?>
                  </div>
                </div>

              </div>
              <!-- LEFT COLOUM -->

              <!-- RIGHT COLOUM -->
              <div class="col-md-6">

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Peminjaman</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control has-feedback-left datepicker" id="tanggal_peminjaman" name="tanggal_peminjaman" placeholder="Tanggal Purchase" aria-describedby="inputSuccess2Status3" value="<?php echo isset($get_data) ? date('d-m-Y',strtotime($get_data->row()->tanggal_peminjaman)) : date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">No Peminjaman</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" placeholder="No Peminjaman" name="peminjaman_number" value="<?php echo isset($get_data) ? $get_data->row()->peminjaman_number : ''; ?>">
                  </div>
                </div>

              </div>
              <!-- RIGHT COLOUM -->

          </div>
          <!-- HEADER SIDE -->

          <!-- DETAIL SIDE -->
          <div class="x_content ItemData">
            <br />

              <div class="col-md-12">

                <div class="row" style="margin-top:20px;">
                  <div class="col-md-12">
                      <h4>Data Buku</h4>
                      <hr />
                  </div>
                </div>

                <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statuspeminjaman == "DRAFT"){?>
                  <div class="row">
                    <div class="col-md-12">
                        <div class="form-group ">
                            <div class="col-md-6">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <tbody>
                                      <tr>
                                        <td>
                                          <select class="form-control" name="id_databuku" id="id_databuku" style="width: !important 80%">
                                            <option value="">-- Select Buku --</option>
                                            <?php foreach ($buku->result() as $baris) { ?>
                                              <option value="<?php echo $baris->id_buku; ?>"><?php echo strtoupper($baris->judul_buku); ?></option>
                                            <?php }?>
                                          </select>
                                        </td>
                                        <td width="50"><button class="btn btn-primary" id="findDataBtn" name="findDataBtn" value="findDataBtn" style="padding-right:20px;" ><i class="fa fa-check"></i> Pilih Buku</button></td>
                                      </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                  </div>  

                  <div class="row">          
                    <div class="col-md-6">
                      <button type="button" name="deletelines" id="deletelines" class="btn btn-sm"><i class="fa fa-times"></i> Hapus Baris Yang Dipilih</button>
                    </div> 
                    <div class="col-md-6" style="text-align:right;">
                      <button type="button" name="initTable" id="initTable" class="btn btn-sm"><i class="fa fa-trash-o"></i> Hapus Seluruh Baris</button>
                    </div> 
                  </div>
                <?php } ?>

                <div class="row">
                  <!-- DETAIL-->
                  <div class="col-md-12"> 
                    <div class="table-scrollablexy">
                      <table class="table table-bordered table-advance table-hover" id="datatables">                        
                        <thead>
                          <tr>
                            <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statuspeminjaman == "DRAFT"){?>
                              <th style="width:8px"></th>
                            <?php } ?>
                            <th><span class="hidden-phone">No.</span></th>
                            <th><span class="hidden-phone">Judul Buku</span></th>
                            <th><span class="hidden-phone">Kategori</span></th>
                            <th><span class="hidden-phone">Penerbit</span></th>
                            <th><span class="hidden-phone">Tahun Cetak</span></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                            $i = 1;
                            if (!empty($get_data_detail)){
                              foreach($get_data_detail->result() as $row){ 
                          ?>
                          <tr class="tblRowSO">
                            <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statuspeminjaman == "DRAFT"){?>
                              <td>
                                <input type="checkbox" class="checkboxes lineitem" value="" name="lineitem" />
                                <input type="hidden" id="id_buku" name="order[<?php echo $i-1;?>][id_buku]" value="<?php echo (isset($row->id_buku)) ? $row->id_buku : '0'; ?>" />
                                <input type="hidden" id="judul_buku" name="order[<?php echo $i-1;?>][judul_buku]" value="<?php echo (isset($row->judul_buku)) ? $row->judul_buku : ''; ?>" />
                                <input type="hidden" id="id_peminjaman" name="order[<?php echo $i-1;?>][id_peminjaman]" value="<?php echo (isset($row->id_peminjaman)) ? $row->id_peminjaman : '0'; ?>" />
                                <input type="hidden" id="id_peminjaman_detail" name="order[<?php echo $i-1;?>][id_peminjaman_detail]" value="<?php echo (isset($row->id_peminjaman_detail)) ? $row->id_peminjaman_detail : '0'; ?>" />
                              </td>
                            <?php } ?>  
                            <td><?php echo $i;?></td> 
                            <td><?php echo $row->judul_buku; ?></td> 
                            <td><?php echo $row->kategori; ?></td> 
                            <td><?php echo $row->penerbit; ?></td>
                            <td>
                              <?php echo $row->tahun_cetak; ?>
                            </td> 
                          </tr>  
                          <?php
                            $i++;
                              }
                            } else {
                          ?>
                          <tr class="tblRowSO">
                            <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statuspeminjaman == "DRAFT"){?>
                              <td>
                                <input type="checkbox" class="checkboxes lineitem" value="" name="lineitem" />
                                <input type="hidden" id="id_buku" name="order[<?php echo $i-1;?>][id_buku]" value="" />
                                <input type="hidden" id="judul_buku" name="order[<?php echo $i-1;?>][judul_buku]" value="" />
                                <input type="hidden" id="id_peminjaman" name="order[<?php echo $i-1;?>][id_peminjaman]" value="" />
                                <input type="hidden" id="id_peminjaman_detail" name="order[<?php echo $i-1;?>][id_peminjaman_detail]" value="" />
                              </td>
                            <?php } ?>
                            <td><?php echo $i;?></td> 
                            <td></td> 
                            <td></td>
                            <td></td> 
                            <td></td>
                          </tr>
                          <?php 
                            }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="ln_solid"></div>
                
              </div>

          </div>
          <!-- DETAIL SIDE -->

          <!-- HEADER SIDE -->
          <div class="x_content">
            <br />

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <input type="hidden" name="id_peminjaman" value="<?php echo isset($get_data) ? $get_data->row()->id_peminjaman : ''; ?>"/>
                      <a href="<?php echo base_url('Peminjaman'); ?>" class="btn btn-primary" name="Back">Back</a>
                      <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statuspeminjaman == "DRAFT"){?>
                        <button type="submit" class="btn btn-success" name="submit">Submit</button>
                      <?php } ?>
                      <?php if(isset($get_data) && $get_data->row()->statuspeminjaman == "OPEN"){?>
                        <a class="btn btn-info" href="<?php echo base_url();?>Pengembalian/add/<?php echo isset($get_data) ? $get_data->row()->id_peminjaman : ''; ?>">
                          <i class="fa fa-undo"></i>
                          Buat Pengembalian Buku
                        </a>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>

          </div>
          <!-- HEADER SIDE -->
          </form>

        </div>
      </div>
    </div>
  </div>
</div>