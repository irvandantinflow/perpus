        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <?php echo $this->load->view('alert');?>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search"></div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>No Peminjaman</th>
                          <th>Anggota</th>
                          <th>Tanggal</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php
                        $no = 1;
                        if ($list != '') :
                        foreach($list->result() as $row){ 
                        ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $row->peminjaman_number; ?></td>
                          <td><?php echo $row->kode_anggota; ?></td>
                          <td><?php echo date('d-m-Y',strtotime($row->tanggal_peminjaman)); ?></td>
                          <td><?php echo $row->statuspeminjaman; ?></td>
                          <td>
                            <a class="glyphicon glyphicon-edit" href="<?php echo base_url();?>Peminjaman/edit/<?php echo $row->id_peminjaman;?>" title="Edit"></a>
                          </td>
                        </tr>
                        <?php } endif;?>
                      </tbody>
                    </table>

                  </div>
                  <div style="text-align: right; margin-top: 20px;">
                    <a class="btn btn-primary" href="<?php echo base_url('Peminjaman/add'); ?>">
                      <i class="fa fa-plus icon-white"></i>
                      Tambah Peminjaman
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->