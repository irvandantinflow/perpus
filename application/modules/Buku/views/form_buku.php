<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left"></div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $title; ?> </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form class="form-horizontal form-label-left" method="POST" enctype="multipart/form-data" action="<?php echo $page_action;?>" >

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Judul Buku</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control" placeholder="Judul Buku" name="judul_buku" value="<?php echo isset($get_data) ? $get_data->row()->judul_buku : ''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Deksripsi Buku</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <textarea id="deskripsi_buku" name="deskripsi_buku" rows="3" class="form-control input-medium"><?php echo isset($get_data) ? $get_data->row()->deskripsi_buku : ''; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <select class="form-control select2_single" name="id_kategori" required="">
                    <option value="">-- Select Kategori --</option>
                    <?php foreach ($kategori->result() as $baris) {
                      if (isset($get_data) && $baris->id_kategori == $get_data->row()->id_kategori) { ?>
                      <option value="<?php echo $baris->id_kategori; ?>" selected><?php echo strtoupper($baris->kategori); ?></option>
                       <?php } else { ?>
                      <option value="<?php echo $baris->id_kategori; ?>"><?php echo strtoupper($baris->kategori); ?></option>
                    <?php } }?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Penerbit</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <select class="form-control select2_single" name="id_penerbit" required="">
                    <option value="">-- Select Penerbit --</option>
                    <?php foreach ($penerbit->result() as $baris) {
                      if (isset($get_data) && $baris->id_penerbit == $get_data->row()->id_penerbit) { ?>
                      <option value="<?php echo $baris->id_penerbit; ?>" selected><?php echo strtoupper($baris->penerbit); ?></option>
                       <?php } else { ?>
                      <option value="<?php echo $baris->id_penerbit; ?>"><?php echo strtoupper($baris->penerbit); ?></option>
                    <?php } }?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun Cetak</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control" placeholder="Tahun Cetak" name="tahun_cetak" value="<?php echo isset($get_data) ? $get_data->row()->tahun_cetak : ''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">File</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name="upload" id="upload" type="file" accept=".pdf, .doc, .docx, .jpg, .png, .jpeg" <?php echo (!isset($get_data) || (isset($get_data->row()->filename) && $get_data->row()->filename == '')) ? 'required=""' : '';?> class="form-control"  >
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">File</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <div id="pdf-thumbnail" >
                    <?php if(isset($get_data) && isset($get_data->row()->filename) && $get_data->row()->filename != ''){
                      echo '<iframe src="'.base_url().'uploads/'.$get_data->row()->filename.'" width="100%" height="350px"></iframe>';
                    } ?>
                  </div>
                  <div id="pdf-thumbnail-filename"></div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Aktif</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <p>
                    YA <input type="radio" class="flat" name="aktif" value="Y" checked="" required <?php echo (isset($get_data) && $get_data->row()->aktif == 'Y') ? 'checked' : ''; ?>/> 
                    TIDAK <input type="radio" class="flat" name="aktif" value="N" <?php echo (isset($get_data) && $get_data->row()->aktif == 'N') ? 'checked' : ''; ?>/>
                  </p>
                </div>
              </div>

              <input type="hidden" name="id_buku" value="<?php echo isset($get_data) ? $get_data->row()->id_buku : ''; ?>"/>

              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                  <a href="<?php echo base_url('Buku'); ?>" class="btn btn-primary" name="cancel">Cancel</a>
                  <button type="submit" class="btn btn-success" name="submit">Submit</button>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>