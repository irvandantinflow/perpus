<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buku_model extends CI_Model 
{    
	var $table = 'buku';
	var $primary = 'id_buku';

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT T0.*, 
        							T1.kategori,
        							T2.penerbit,
        							CASE T0.aktif WHEN 'Y' THEN 'YA' ELSE 'TIDAK' END AS status_aktif 
        							FROM buku T0
        							LEFT JOIN kategori T1 ON T0.id_kategori = T1.id_kategori
        							LEFT JOIN penerbit T2 ON T0.id_penerbit = T2.id_penerbit
        							order by T0.judul_buku asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function save($id ='', $data =''){

    	if(!empty($data)){
    		$this->db->where($this->primary, $id);
			$is_check = $this->db->get($this->table);

			if($is_check->num_rows() > 0){

				$this->db->set($data);
				$this->db->set('updated_user', $this->session->userdata('user_id'));
				$this->db->set('updated_date', 'now()', FALSE);
				$this->db->where($this->primary, $id);
				
				$query = $this->db->update($this->table);
				if($query)
					return true;
				else
					return false;
			} else {
				$this->db->set('created_user', $this->session->userdata('user_id'));
				$this->db->set('created_date', 'now()', FALSE);
				$query = $this->db->insert($this->table, $data);
				if($query)
					return true;
				else
					return false;
			}
    	}  	
    }

    public function delete($id){
		$query = $this->db->delete($this->table,array($this->primary=>$id));
		if($query)
			return true;
		else
			return false;
	}

	function checkID($id){
		$query = $this->db->get_where($this->table, array($this->primary=>$id));
		if ($query->num_rows() > 0) 
			return true;
		else
			return false;
    }

    public function edit($id){
		$query = $this->db->get_where($this->table, array($this->primary=>$id));
		return $query;
	}

	function getBukuAktif(){
        $query = $this->db->query("SELECT * from buku where aktif = 'Y' order by judul_buku asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function getBukuAll(){
        $query = $this->db->query("SELECT * from buku order by judul_buku asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function search_item($id=''){

    	$ret = '';  
    	$sql = "
    			SELECT a.id_buku, a.judul_buku, a.deskripsi_buku, a.tahun_cetak, 
    			b.kategori,
    			c.penerbit
    			FROM buku a
                left join kategori b on a.id_kategori = b.id_kategori
                left join penerbit c on a.id_penerbit = c.id_penerbit
    			WHERE a.id_buku  = ".$id." and a.aktif = 'Y'
    			";

    	$q = $this->db->query($sql);

    	if ($q->num_rows() > 0) {
    		foreach($q->result() as $row) {
                $ret[] = array("id_buku" 			=> $row->id_buku, 
                               "judul_buku" 		=> $row->judul_buku,
                               "deskripsi_buku"		=> $row->deskripsi_buku,
                               "tahun_cetak"		=> $row->tahun_cetak,
                               "kategori"			=> $row->kategori,
                               "penerbit"			=> $row->penerbit
                           );
            }
    	}

    	return $ret;

    }

}   
