<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Buku_model');
		$this->load->model('Kategori/Kategori_model');
		$this->load->model('Penerbit/Penerbit_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Master',
	   		'content'	=> 'Buku/list_buku',
	   		'title'		=> 'Buku',
	   		'list'		=> $this->Buku_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Buku/form_buku',
		   	'title'			=> 'New Buku',
		   	'kategori'		=> 	$this->Kategori_model->getKategoriAktif(),
		   	'penerbit'		=> 	$this->Penerbit_model->getPenerbitAktif(),
	   		'page_action' 	=> base_url('Buku/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){
		
		$this->form_validation->set_rules('judul_buku','Buku', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));	

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Buku');

		} else {
			
			$data = array(
						'judul_buku'			=> $accept['judul_buku'],
						'deskripsi_buku'		=> $accept['deskripsi_buku'],
						'id_kategori'			=> $accept['id_kategori'],
						'id_penerbit'			=> $accept['id_penerbit'],
						'tahun_cetak'			=> $accept['tahun_cetak'],
						'aktif'					=> $accept['aktif']);

			if(!empty($_FILES['upload']['name'])){
				$fileBook = $this->doUploadFile();

				if($fileBook)
				{
					$data += array('filename' => $fileBook['file_name']);
				}
			}
			
			$query = $this->Buku_model->save($accept['id_buku'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Buku');
		}	
	}

	function doUploadFile(){
		$config['upload_path'] = 'uploads/';
		$config['max_size'] = '60000';
        $config['allowed_types'] = 'pdf|doc|docx|jpg|png|jpeg';
        $config['overwrite'] = FALSE;
	    $config['encrypt_name'] = TRUE;
	    $config['remove_spaces'] = FALSE;
	    if ( ! is_dir($config['upload_path']) ) {
	    	$this->session->set_flashdata('error','THE UPLOAD DIRECTORY DOES NOT EXIST');
	    	return false;
	    }
	    $this->load->library('upload', $config);
	    if ( ! $this->upload->do_upload('upload')) {
	    	$this->session->set_flashdata('error',$this->upload->display_errors('<p>', '</p>'));
	        return false;
	    } else {
	        $data = $this->upload->data();
			return $data;
	    }		
	}

	function edit($id = 0){	
		if(!$this->Buku_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Buku');
		};

	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Buku/form_buku',
	   		'title'			=> 'Edit Buku',
	   		'page_action' 	=> base_url('Buku/save'),
	   		'kategori'		=> 	$this->Kategori_model->getKategoriAktif(),
		   	'penerbit'		=> 	$this->Penerbit_model->getPenerbitAktif(),
	   		'get_data'		=> $this->Buku_model->edit($id),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function searchBuku($searchBuku=''){ 
		$searchBuku = urldecode($searchBuku);
		$retData  = $this->Buku_model->search_item($searchBuku);
		
		echo json_encode($retData);
	}
}