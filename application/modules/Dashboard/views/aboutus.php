<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>E-Library :: Library University of Kita Bersama</title>

    <!--== Favicon ==-->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/assets/img/favicon.ico" type="image/x-icon"/>

    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700" rel="stylesheet">

    <!--== All Animate CSS ==-->
    <link href="<?php echo base_url()?>assets/assets/css/animate.min.css" rel="stylesheet"/>
    <!--== All FontAwesome CSS ==-->
    <link href="<?php echo base_url()?>assets/assets/css/font-awesome.min.css" rel="stylesheet"/>
    <!--== All Material Icons CSS ==-->
    <link href="<?php echo base_url()?>assets/assets/css/materialdesignicons.min.css" rel="stylesheet"/>
    <!--== All Helper CSS ==-->
    <link href="<?php echo base_url()?>assets/assets/css/helper.min.css" rel="stylesheet"/>
    <!--== All Revolution CSS ==-->
    <link href="<?php echo base_url()?>assets/assets/css/settings.css" rel="stylesheet"/>
    <!--== All Slicknav CSS ==-->
    <link href="<?php echo base_url()?>assets/assets/css/slicknav.min.css" rel="stylesheet"/>
    <!--== All Timeline CSS ==-->
    <link href="<?php echo base_url()?>assets/assets/css/timeline.css" rel="stylesheet"/>
    <!--== All Slick Slider CSS ==-->
    <link href="<?php echo base_url()?>assets/assets/css/slick.min.css" rel="stylesheet"/>
    <!--== All BootStrap CSS ==-->
    <link href="<?php echo base_url()?>assets/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <!--== Main Style CSS ==-->
    <link href="<?php echo base_url()?>assets/assets/css/style.min.css" rel="stylesheet"/>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!--== Start Header Area Wrapper ==-->
<header class="header-area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-5 col-lg-2">
                <!-- Start Logo Area -->
                <div class="logo-area">
                    <a href="/.">Perpustakaan</a>
                </div>
                <!-- End Logo Area -->
            </div>

            <div class="col-lg-7 d-none d-lg-block">
                <!-- Start Navigation Area -->
                <div class="navigation-area mt-lg-3">
                    <ul class="main-menu nav">
                        <li><a href="/.">Beranda</a></li>
                        <li><a href="<?php echo base_url()?>dashboard/katalog">Katalog</a></li>
                        <li><a href="#">About Us</a>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </div>
                <!-- End Navigation Area -->
            </div>

            <div class="col-7 col-lg-3">
                <!-- Start Header Action Area -->
                <div class="header-action mt-lg-3 text-right">
                    <a href="tel:00199823568658" class="tel-no">+62 8</a>
                    <button class="btn-cog"><i class="fa fa-cog"></i></button>
                    <button class="btn-menu d-lg-none"><i class="fa fa-bars"></i></button>
                </div>
                <!-- End Header Action Area -->
            </div>
        </div>
    </div>
</header>
<!--== End Header Area Wrapper ==-->

<!--== Start Page Header Area ==-->
<div class="page-header-area bg-img" data-bg="<?php echo base_url()?>assets/assets/img/page-header.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-xl-8 m-auto text-center">
                <div class="page-header-content-inner">
                    <div class="page-header-content">
                        <h2>ABOUT US</h2>
                        <p>Perpustakaan Online.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End Page Header Area ==-->

<!--== Start Page Content Area Wrapper ==-->
<div class="page-content-wrapper">
    <div class="member-details-wrapper sm-top">
        <div class="container">
            <div class="member-details-wrap">
                <div class="member-details-top">
                    <div class="row">

                        <div class="col-md-12 ml-auto">
                            <div class="member-desc">
                                <h2>About Us</h2>
                                <h5>Telkom University Open Library</h5>
                                <p>Telkom University Open Library adalah brand untuk Unit Sumber Daya Keilmuan & Perpustakaan (SDK & Perpustakaan) Telkom University yang berada di bawah Wakil Rektor III. Telkom University Open Library memiliki visi "Menjadi leader dari pusat ilmu dan pengetahuan berbasis teknologi informasi"
                                </p>
                                <p>Untuk mencapai visi tersebut, maka SDK &amp; Perpustakaan Telkom University mengusung misi sebagai berikut:</p>
<p>1. Berperan aktif dalam melakukan akuisisi pengetahuan, mengelola pengetahuan, dan berbagi pengetahuan.</p>
<p>2. Berperan aktif dalam meningkatkan minat baca dan tulis di masyarakat</p>
<p>3. Bekerja sama dengan semua instansi dalam pengembangan ilmu dan pengetahuan.</p>
<p>&nbsp;</p>
<p>Telkom University Open Library memiliki lebih dari 80.500 judul koleksi dengan jumlah eksemplar 123.937 eksemplar, dan telah mendapatkan akreditasi "A" dari Perpustakaan Nasional RI pada tahun 2015. Sejak 2014 SDK &amp; Perpustakaan Telkom University telah mengembangkan konsep "open library" yang terdiri dari 3 (tiga) pokok pemikiran, yaitu:</p>
<p>1. Open untuk menerima semua jenis knowledge</p>
<p>2. Open untuk berbagi knowledge dengan knowledge management lain</p>
<p>3. Open untuk siapa pun yang ingin belajar</p>
<p>&nbsp;</p>
<p>Dalam pelaksanaan operasionalnya, konsep "Open Library" didukung oleh pengembangan teknologi informasi sistem perpustakaan, untuk senantiasa meningkatkan layanan, database, dan koleksi perpustakaan. Telkom University juga ingin perpustakaannya dapat bermanfaat sebesar mungkin dan sebanyak mungkin bagi sivitas dan warga Telkom University serta masyarakat luas. Oleh karena itu, Telkom University Open Library menggagas juga kegiatan-kegiatan untuk menggerakkan literasi seperti "Telkom University Literacy Event", "Library Open Discussion", penggalangan donasi buku, program perpustakaan binaan di beberapa daerah terpencil, serta mengedukasi anti-plagiarisme.</p>
<p>&nbsp;</p>
<ol> </ol>
<p><strong>Stuktur Organisasi</strong></p>
<p>&nbsp;</p>
<p><img src="/assets/struktur_organisasi.png" alt="" /></p>
<p>&nbsp;</p>
<p>SDK &amp; Perpustakaan Telkom University dikelola oleh seorang Kepala Bagian/ Unit yang membawahi dua urusan pekerjaan yaitu Urusan Pengembangan Bahan Pustaka dan Database (bertugas untuk merencanakan, melaksanakan, mengendalikan, dan mengembangkan database dan bahan pustaka) dan Urusan Administrasi dan Layanan Pustaka (bertugas untuk merencanakan, melaksanakan, mengendalikan dan mengelola administrasi dan layanan di perpustakaan) dengan jumlah staf perpustakaan sebanyak 12 orang terdiri dari pustakawan dan staf non-pustakawan.<br /><br /><br /><br /></p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End Page Content Area Wrapper ==-->

<!--== Start Fun Fact Area Wrapper ==-->
<div class="fun-fact-area sm-top parallax" data-parallax-speed="0.70" data-bg="assets/assets/img/fun-fact-bg.jpg">
    <div class="container">

    </div>
</div>
<!--== End Fun Fact Area Wrapper ==-->

<!--== Start Footer Area Wrapper ==-->
<footer class="footer-area sp-y">

    <div class="footer-widget-area sm-top-wt">
        <div class="container">
            <div class="row mtn-40">
                <div class="col-lg-4 order-4 order-lg-0">
                    <div class="widget-item">
                        <div class="<?php echo base_url()?>about-widget">
                            <a href="index.html"><img src="<?php echo base_url()?>assets/assets/img/logo-dark.png" alt="Logo"/></a>
                            <p>During the summer my wife and I got to go on an amazing road trip in Vancouver.</p>

                            <div class="copyright-txt">
                                <p>&copy; 2019 Perpus Ltd. All Rights Reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-2 ml-auto">
                    <div class="widget-item">
                        <h4 class="widget-title">Kampus </h4>
                        <div class="widget-body">
                            <ul class="widget-list">
                                <li><a href="#">Our company</a></li>
                                <li><a href="#">Contact us</a></li>
                                <li><a href="#">Our services</a></li>
                                <li><a href="#">Careers</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-3">
                    <div class="widget-item">
                        <h4 class="widget-title">Contact</h4>
                        <div class="widget-body">
                            <address>
                                2005 Stokes Isle Apartment. 896, Washington 10010, USA <br>
                                https://example.com <br>
                                (+68) 120034509
                            </address>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--== End Footer Area Wrapper ==-->

<!-- Scroll Top Button -->
<button class="btn-scroll-top"><i class="mdi mdi-chevron-up"></i></button>

        <?php  
        if($this->session->flashdata('error')){
            $error = $this->session->flashdata('message') ? $this->session->flashdata('message') : $this->session->flashdata('error');
            echo "<script>alert('".$error."')</script>";
        }elseif($this->session->flashdata('success') || $this->session->flashdata('message')){
            $message = $this->session->flashdata('message') ? $this->session->flashdata('message') : $this->session->flashdata('success');
            echo "<script>alert('".$message."')</script>";

        }?>
    
<!-- Start Off Canvas Menu Wrapper -->
<aside class="off-canvas-wrapper off-canvas-cog">
    <div class="off-canvas-overlay"></div>
    <div class="off-canvas-inner">
        <div class="close-btn">
            <button class="btn-close"><i class="mdi mdi-close"></i></button>
        </div>

        <!-- Start Off Canvas Content -->
        <div class="off-canvas-content mb-sm-30">
            <div class="off-canvas-item">                
                <div class="log-in-content-wrap">
                    <h2>Login</h2>
                    <div class="login-form mtn-15">
                        <form action="<?php echo $page_action; ?>" method="post">
                            <?php //echo form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash()); ?>
                            <div class="form-input-item">
                                <label for="username" class="sr-only">Username</label>
                                <input type="text" id="username" placeholder="Kode Anggota" name="kode_anggota" required>
                            </div>

                            <div class="form-input-item">
                                <label for="password" class="sr-only">Password</label>
                                <input type="password" id="password" placeholder="Password" name="dob" required>
                            </div>

                            <div class="form-input-item">
                                <button type="submit" class="btn-submit">Login</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <div class="off-canvas-item mt-sm-30">
                <div class="social-icons">
                    <a href="https://facebook.com" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="https://instagram.com" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://dribbble.com" target="_blank"><i class="fa fa-dribbble"></i></a>
                    <a href="https://pinterest.com" target="_blank"><i class="fa fa-pinterest"></i></a>
                </div>
                <div class="copyright-content">
                    <p> © Perpus 2019. All Right Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</aside>
<!-- End Off Canvas Menu Wrapper -->


<!-- Start Off Canvas Menu Wrapper -->
<aside class="off-canvas-wrapper off-canvas-menu">
    <div class="off-canvas-overlay"></div>
    <div class="off-canvas-inner">
        <!-- Start Off Canvas Header -->
        <div class="close-btn">
            <button class="btn-close"><i class="mdi mdi-close"></i></button>
        </div>

        <!-- Start Off Canvas Content -->
        <div class="off-canvas-content">
            <div class="res-mobile-menu">

            </div>
        </div>
    </div>
</aside>
<!-- End Off Canvas Menu Wrapper -->


<!--=======================Javascript============================-->
<script src="<?php echo base_url()?>assets/assets/js/modernizr-3.6.0.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/popper.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/plugins/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/plugins/waypoint.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/plugins/counterup.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/plugins/instafeed.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/plugins/jquery.appear.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/plugins/jquery.slicknav.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/plugins/parallax.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/plugins/slick.min.js"></script>

<!--=== Active Js ===-->
<script src="<?php echo base_url()?>assets/assets/js/active.min.js"></script>

<!-- REVOLUTION JS FILES -->
<script src="<?php echo base_url()?>assets/assets/js/revslider/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/revslider/jquery.themepunch.revolution.min.js"></script>


<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="<?php echo base_url()?>assets/assets/js/revslider/extensions/revolution.extension.actions.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/revslider/extensions/revolution.extension.carousel.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/revslider/extensions/revolution.extension.kenburn.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/revslider/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/revslider/extensions/revolution.extension.migration.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/revslider/extensions/revolution.extension.navigation.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/revslider/extensions/revolution.extension.parallax.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/revslider/extensions/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/js/revslider/extensions/revolution.extension.video.min.js"></script>

<!--=== REVOLUTION JS ===-->
<script src="<?php echo base_url()?>assets/assets/js/revslider/rev-active.js"></script>

</body>

</html>