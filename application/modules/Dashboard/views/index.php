<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>E-Library :: Library University of Kita Bersama</title>

    <!--== Favicon ==-->
    <link rel="shortcut icon" href="assets/assets/img/favicon.ico" type="image/x-icon"/>

    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700" rel="stylesheet">

    <!--== All Animate CSS ==-->
    <link href="assets/assets/css/animate.min.css" rel="stylesheet"/>
    <!--== All FontAwesome CSS ==-->
    <link href="assets/assets/css/font-awesome.min.css" rel="stylesheet"/>
    <!--== All Material Icons CSS ==-->
    <link href="assets/assets/css/materialdesignicons.min.css" rel="stylesheet"/>
    <!--== All Helper CSS ==-->
    <link href="assets/assets/css/helper.min.css" rel="stylesheet"/>
    <!--== All Revolution CSS ==-->
    <link href="assets/assets/css/settings.css" rel="stylesheet"/>
    <!--== All Slicknav CSS ==-->
    <link href="assets/assets/css/slicknav.min.css" rel="stylesheet"/>
    <!--== All Timeline CSS ==-->
    <link href="assets/assets/css/timeline.css" rel="stylesheet"/>
    <!--== All Slick Slider CSS ==-->
    <link href="assets/assets/css/slick.min.css" rel="stylesheet"/>
    <!--== All BootStrap CSS ==-->
    <link href="assets/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <!--== Main Style CSS ==-->
    <link href="assets/assets/css/style.min.css" rel="stylesheet"/>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!--== Start Header Area Wrapper ==-->
<header class="header-area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-5 col-lg-2">
                <!-- Start Logo Area -->
                <div class="logo-area">
                    <a href="/.">Perpustakaan</a>
                </div>
                <!-- End Logo Area -->
            </div>

            <div class="col-lg-7 d-none d-lg-block">
                <!-- Start Navigation Area -->
                <div class="navigation-area mt-lg-3">
                    <ul class="main-menu nav">
                        <li><a href="/.">Beranda</a></li>
                        <li><a href="<?php echo base_url()?>dashboard/katalog">Katalog</a></li>
                        <li><a href="<?php echo base_url()?>dashboard/aboutus">About Us</a>
                        </li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </div>
                <!-- End Navigation Area -->
            </div>

            <div class="col-7 col-lg-3">
                <!-- Start Header Action Area -->
                <div class="header-action mt-lg-3 text-right">
                    <a href="tel:00199823568658" class="tel-no">+62 8</a>
                    <button class="btn-cog"><i class="fa fa-cog"></i></button>
                    <button class="btn-menu d-lg-none"><i class="fa fa-bars"></i></button>
                </div>
                <!-- End Header Action Area -->
            </div>
        </div>
    </div>
</header>
<!--== End Header Area Wrapper ==-->

<!--== Start Slider Area Wrapper ==-->
<div class="slider-area-wrapper">
    <div id="rev_slider_11_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="business-classic"
         data-source="gallery">
        <div id="rev_slider_11_1" class="rev_slider fullwidthabanner" data-version="5.4.7">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-27" data-transition="random-premium" data-slotamount="default" data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
                    data-thumb="assets/asklibrarian_cover.jpg" data-rotate="0" data-saveperformance="off"
                    data-title="Slide">
                    <!-- MAIN IMAGE -->
                    <img src="assets/asklibrarian_cover.jpg" alt="Businex" data-bgposition="center center" data-bgfit="cover"
                         data-bgrepeat="no-repeat" data-bgparallax="15" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-resizeme slide-heading" data-x="['left','left','left','left']"
                         data-hoffset="['135','135','40','40']" data-y="['top','top','top','top']"
                         data-voffset="['250','204','200','201']" data-fontsize="['60','60','50','30']"
                         data-fontweight="['600']" data-lineheight="['70','70','60','40']"
                         data-width="['650','650','600','320']" data-height="none" data-whitespace="normal"
                         data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":10,"split":"lines","splitdelay":0.1,"speed":600,"split_direction":"forward","frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left']">
                        Perpustakaan Online <br> Kampus Kita Bersama
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption tp-resizeme slide-txt" data-x="['left','left','left','left']"
                         data-hoffset="['135','135','40','40']" data-y="['top','top','top','top']"
                         data-voffset="['400','370','370','300']" data-fontsize="['16','20','20','20']"
                         data-fontweight="['400','400','400','400']" data-width="['600','600','600','320']"
                         data-height="none" data-whitespace="normal" data-visibility="['on','on','on','on']"
                         data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":360,"split":"lines","splitdelay":0.1,"speed":500,"split_direction":"forward","frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                        Businex always try to provide the best Business Solutions for Clinets to grow up their Business
                        very sharply and smoothly.

                    </div>

                    <!-- LAYER NR. 3 -->
                    <!-- <div class="tp-caption Button-Outline-Secondary rev-btn" id="slide-27-layer-11"
                         data-x="['left','left','left','left']" data-hoffset="['135','135','45','40']"
                         data-y="['top','top','top','top']" data-voffset="['490','470','490','465']" data-width="none"
                         data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on"
                         data-responsive="off"
                         data-frames='[{"delay":650,"speed":500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"350","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255,255,255,1);bg:rgb(8, 11, 26);"}]'
                         data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[40,40,40,30]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[40,40,40,30]">Learn More
                    </div> -->
                </li>

                <!-- SLIDE  >
                <li data-index="rs-28" data-transition="slidingoverlayvertical,slidingoverlayleft,slideoverup"
                    data-slotamount="default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"
                    data-easein="default,default,default" data-easeout="default,default,default"
                    data-masterspeed="default,default,default" data-thumb="assets/img/slider/02_thumb.jpg"
                    data-rotate="0,0,0" data-saveperformance="off" data-title="Slide">
                    <!-- MAIN IMAGE >
                    <img src="assets/img/slider/02.jpg" alt="Businex" data-bgposition="center center" data-bgfit="cover"
                         data-bgrepeat="no-repeat" data-bgparallax="15" class="rev-slidebg" data-no-retina>

                    <!-- LAYER NR. 4 >
                    <div class="tp-caption tp-resizeme slide-heading" data-x="['left','left','left','left']"
                         data-hoffset="['135','135','40','40']" data-y="['top','top','top','top']"
                         data-voffset="['250','204','200','201']" data-fontsize="['60','60','50','30']"
                         data-fontweight="['600']" data-lineheight="['70','70','60','40']"
                         data-width="['650','650','600','320']" data-height="none" data-whitespace="normal"
                         data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":10,"split":"chars","splitdelay":0.1,"speed":1000,"split_direction":"forward","frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left']">
                        INVEST MONEY <br> WITH US
                    </div>

                    <!-- LAYER NR. 5 >
                    <div class="tp-caption tp-resizeme slide-txt" data-x="['left','left','left','left']"
                         data-hoffset="['135','135','40','40']" data-y="['top','top','top','top']"
                         data-voffset="['400','370','370','300']" data-fontsize="['16','20','20','20']"
                         data-fontweight="['400','400','400','400']" data-width="['600','600','600','320']"
                         data-height="none" data-whitespace="normal" data-visibility="['on','on','on','on']"
                         data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":360,"split":"lines","splitdelay":0.1,"speed":500,"split_direction":"forward","frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                        Businex always try to provide the best Business Solutions for Clinets to grow up their Business
                        very sharply and smoothly.

                    </div>

                    <!-- LAYER NR. 6 >
                    <div class="tp-caption Button-Outline-Secondary rev-btn" data-x="['left','left','left','left']"
                         data-hoffset="['135','135','45','40']" data-y="['top','top','top','top']"
                         data-voffset="['490','470','490','465']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off"
                         data-frames='[{"delay":650,"speed":500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"350","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255,255,255,1);bg:rgb(8, 11, 26);"}]'
                         data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[40,40,40,30]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[40,40,40,30]">Learn More
                    </div>
                </li-->
            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
    </div>
</div>
<!--== End Slider Area Wrapper ==-->

<!--== Start About Area Wrapper ==-->
<div class="about-area-wrapper sm-top">
    <div class="container">
        <div class="row align-items-lg-center">
            <div class="col-md-6 col-lg-5">
                <figure class="about-thumb">
                    <img src="assets/struktur_organisasi.png" alt="Businex-About"/>
                </figure>
            </div>

            <div class="col-md-6 col-lg-7">
                <div class="about-content">
                    <h6>ABOUT US</h6>
                    <h2>Kita Bersama University <br> E-Library</h2>
                    <span class="about-since">Since 2014</span>
                    <p>Telkom University Open Library adalah brand untuk Unit Sumber Daya Keilmuan & Perpustakaan (SDK & Perpustakaan) Telkom University yang berada di bawah Wakil Rektor III. Telkom University Open Library memiliki visi "Menjadi leader dari pusat ilmu dan pengetahuan berbasis teknologi informasi".</p>
                    <a href="<?php echo base_url()?>dashboard/aboutus" class="btn-about">MORE DETAILS <i class="mdi mdi-chevron-double-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End About Area Wrapper ==-->

<!--== Start Service Area Wrapper ==-->
<div class="service-area-wrapper sm-top-wt">
    <div class="service-area-top parallax" data-parallax-speed="0.75" data-bg="assets/assets/img/service/service-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xl-5 m-auto text-center">
                    <div class="section-title section-title--light">
                        <h6>New Book</h6>
                        <h2 class="mb-0">New Book in Library</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="service-content-area">
        <div class="container">
            <div class="row mtn-30">
                <?php 
                foreach ($newbook->result() as $nb) {
                    $text = $nb->deskripsi_buku;
                    $num_char = strlen($text) > 100 ? 100 : strlen($text);
                    $cut_text = substr($text, 0, $num_char);
                    if ($text{$num_char - 1} != ' ') { // jika huruf ke 50 (50 - 1 karena index dimulai dari 0) buka  spasi
                        $new_pos = strrpos($cut_text, ' '); // cari posisi spasi, pencarian dari huruf terakhir
                        $cut_text = substr($text, 0, $new_pos);
                    }
                    echo '<div class="col-sm-6 col-lg-4">
                    <!-- Start Service Item -->
                    <div class="service-item">
                        <figure class="service-thumb">
                            <a href="service-details.html"><img src="uploads/'.$nb->filename.'" alt="Businex-Service"/></a>

                            <figcaption class="service-txt">
                                <h5>'.$nb->judul_buku.'</h5>
                            </figcaption>
                        </figure>
                        <div class="service-content">
                            <div class="service-content-inner">
                                <h5><a href="service-details.html" class="stretched-link"></a>'.$nb->judul_buku.'</h5>
                                <p>'.$cut_text.'</p>
                            </div>
                        </div>
                    </div>
                    <!-- End Service Item -->
                </div>';
                }

                ?>

            </div>
        </div>
    </div>
</div>
<!--== End Service Area Wrapper ==-->

<!--== Start Testimonial Area Wrapper ==-->
<div class="testimonial-area bg-offwhite sp-y sm-top">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="section-title mb-0">
                    <h6>TESTIMONIALS</h6>
                    <h2>Ulasan Terbaru <br>dari Para Mahasiswa</h2>
                    <p><strong>Ulasan</strong> Pentingnya sebuah ulasan.</p>
                </div>

                <div class="testimonial-carousel-dots"></div>
            </div>

            <div class="col-lg-6 ml-auto">
                <div class="testimonial-content-wrap">
                    <div class="testimonial-content">
                        <?php

                        foreach ($newtesti->result() as $nt) {
                            echo '<!-- Start Testimonial Item -->
                        <div class="testimonial-item">
                            <div class="testimonial-thumb">
                                <img src="uploads/'.$nt->filename.'" alt="Businex"/>
                            </div>

                            <div class="testimonial-txt">
                                <a href="service-details.html">'.$nt->judul_buku.'</a>
                                <p><div class="starrr stars-existing">';
                                for($i = 0; $i < $nt->book_rate; $i++){
                                  echo '<a href="#" class="fa fa-star"></a>';
                                }
                                for($i = 0; $i < (5 - $nt->book_rate); $i++){ 
                                  echo '<a href="#" class="fa fa-star-o"></a>';
                                } 
                                echo '
                                </div>'.date('d-m-Y',strtotime($nt->created_date)).'
                                </p>
                                <p>'.$nt->remarks.'</p>
                                <h5 class="client-name">'.$nt->nama_anggota.'</h5>
                            </div>
                        </div>
                        <!-- End Testimonial Item -->';
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End Testimonial Area Wrapper ==-->

<!--== Start Fun Fact Area Wrapper ==-->
<div class="fun-fact-area sm-top parallax" data-parallax-speed="0.70" data-bg="assets/assets/img/fun-fact-bg.jpg">
    <div class="container">

    </div>
</div>
<!--== End Fun Fact Area Wrapper ==-->

<!--== Start Footer Area Wrapper ==-->
<footer class="footer-area sp-y">

    <div class="footer-widget-area sm-top-wt">
        <div class="container">
            <div class="row mtn-40">
                <div class="col-lg-4 order-4 order-lg-0">
                    <div class="widget-item">
                        <div class="about-widget">
                            <a href="index.html"><img src="assets/assets/img/logo-dark.png" alt="Logo"/></a>
                            <p>During the summer my wife and I got to go on an amazing road trip in Vancouver.</p>

                            <div class="copyright-txt">
                                <p>&copy; 2019 Perpus Ltd. All Rights Reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-2 ml-auto">
                    <div class="widget-item">
                        <h4 class="widget-title">Kampus </h4>
                        <div class="widget-body">
                            <ul class="widget-list">
                                <li><a href="#">Our company</a></li>
                                <li><a href="#">Contact us</a></li>
                                <li><a href="#">Our services</a></li>
                                <li><a href="#">Careers</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-3">
                    <div class="widget-item">
                        <h4 class="widget-title">Contact</h4>
                        <div class="widget-body">
                            <address>
                                2005 Stokes Isle Apartment. 896, Washington 10010, USA <br>
                                https://example.com <br>
                                (+68) 120034509
                            </address>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--== End Footer Area Wrapper ==-->

<!-- Scroll Top Button -->
<button class="btn-scroll-top"><i class="mdi mdi-chevron-up"></i></button>

        <?php  
        if($this->session->flashdata('error')){
            $error = $this->session->flashdata('message') ? $this->session->flashdata('message') : $this->session->flashdata('error');
            echo "<script>alert('".$error."')</script>";
        }elseif($this->session->flashdata('success') || $this->session->flashdata('message')){
            $message = $this->session->flashdata('message') ? $this->session->flashdata('message') : $this->session->flashdata('success');
            echo "<script>alert('".$message."')</script>";

        }?>
    
<!-- Start Off Canvas Menu Wrapper -->
<aside class="off-canvas-wrapper off-canvas-cog">
    <div class="off-canvas-overlay"></div>
    <div class="off-canvas-inner">
        <div class="close-btn">
            <button class="btn-close"><i class="mdi mdi-close"></i></button>
        </div>

        <!-- Start Off Canvas Content -->
        <div class="off-canvas-content mb-sm-30">
            <div class="off-canvas-item">                
                <div class="log-in-content-wrap">
                    <h2>Login</h2>
                    <div class="login-form mtn-15">
                        <form action="<?php echo $page_action; ?>" method="post">
                            <?php //echo form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash()); ?>
                            <div class="form-input-item">
                                <label for="username" class="sr-only">Username</label>
                                <input type="text" id="username" placeholder="Kode Anggota" name="kode_anggota" required>
                            </div>

                            <div class="form-input-item">
                                <label for="password" class="sr-only">Password</label>
                                <input type="password" id="password" placeholder="Password" name="dob" required>
                            </div>

                            <div class="form-input-item">
                                <button type="submit" class="btn-submit">Login</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <div class="off-canvas-item mt-sm-30">
                <div class="social-icons">
                    <a href="https://facebook.com" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="https://instagram.com" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://dribbble.com" target="_blank"><i class="fa fa-dribbble"></i></a>
                    <a href="https://pinterest.com" target="_blank"><i class="fa fa-pinterest"></i></a>
                </div>
                <div class="copyright-content">
                    <p> © Perpus 2019. All Right Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</aside>
<!-- End Off Canvas Menu Wrapper -->


<!-- Start Off Canvas Menu Wrapper -->
<aside class="off-canvas-wrapper off-canvas-menu">
    <div class="off-canvas-overlay"></div>
    <div class="off-canvas-inner">
        <!-- Start Off Canvas Header -->
        <div class="close-btn">
            <button class="btn-close"><i class="mdi mdi-close"></i></button>
        </div>

        <!-- Start Off Canvas Content -->
        <div class="off-canvas-content">
            <div class="res-mobile-menu">

            </div>
        </div>
    </div>
</aside>
<!-- End Off Canvas Menu Wrapper -->


<!--=======================Javascript============================-->
<script src="assets/assets/js/modernizr-3.6.0.min.js"></script>
<script src="assets/assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/assets/js/popper.min.js"></script>
<script src="assets/assets/js/plugins/bootstrap.min.js"></script>
<script src="assets/assets/js/plugins/waypoint.min.js"></script>
<script src="assets/assets/js/plugins/counterup.min.js"></script>
<script src="assets/assets/js/plugins/instafeed.min.js"></script>
<script src="assets/assets/js/plugins/jquery.appear.js"></script>
<script src="assets/assets/js/plugins/jquery.slicknav.min.js"></script>
<script src="assets/assets/js/plugins/parallax.min.js"></script>
<script src="assets/assets/js/plugins/slick.min.js"></script>

<!--=== Active Js ===-->
<script src="assets/assets/js/active.min.js"></script>

<!-- REVOLUTION JS FILES -->
<script src="assets/assets/js/revslider/jquery.themepunch.tools.min.js"></script>
<script src="assets/assets/js/revslider/jquery.themepunch.revolution.min.js"></script>


<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="assets/assets/js/revslider/extensions/revolution.extension.actions.min.js"></script>
<script src="assets/assets/js/revslider/extensions/revolution.extension.carousel.min.js"></script>
<script src="assets/assets/js/revslider/extensions/revolution.extension.kenburn.min.js"></script>
<script src="assets/assets/js/revslider/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="assets/assets/js/revslider/extensions/revolution.extension.migration.min.js"></script>
<script src="assets/assets/js/revslider/extensions/revolution.extension.navigation.min.js"></script>
<script src="assets/assets/js/revslider/extensions/revolution.extension.parallax.min.js"></script>
<script src="assets/assets/js/revslider/extensions/revolution.extension.slideanims.min.js"></script>
<script src="assets/assets/js/revslider/extensions/revolution.extension.video.min.js"></script>

<!--=== REVOLUTION JS ===-->
<script src="assets/assets/js/revslider/rev-active.js"></script>

</body>

</html>