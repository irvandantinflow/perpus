<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model 
{
    public function __construct(){
        parent::__construct(); 
    }
    
    public function newbook(){
    	$query = $this->db->query("SELECT A.*, B.kategori, C.penerbit FROM buku A 
					INNER JOIN kategori B ON A.id_kategori=B.id_kategori
					INNER JOIN penerbit C ON A.id_penerbit=C.id_penerbit
					ORDER BY A.tahun_cetak DESC, A.created_date DESC limit 6");
    	if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function newtesti(){
    	$query = $this->db->query("SELECT A.*,B.nama_anggota,C.filename FROM review A
					INNER JOIN anggota B ON A.id_anggota=B.id_anggota
					INNER JOIN buku C ON A.id_buku=C.id_buku
					ORDER BY A.created_date");
    	if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function kataloglist($number=0,$offset=0){
    	if($number <= 0){
	    	$query = $this->db->query("SELECT A.*, B.kategori, C.penerbit FROM buku A 
						INNER JOIN kategori B ON A.id_kategori=B.id_kategori
						INNER JOIN penerbit C ON A.id_penerbit=C.id_penerbit
						ORDER BY A.tahun_cetak DESC, A.created_date DESC");
	    	if($query->num_rows()>0){
	            return $query;
	        } else {
	            $query->free_result();
	            return $query;
	        }
	    }else{
	    	$query = $this->db->query("SELECT A.*, B.kategori, C.penerbit FROM buku A 
						INNER JOIN kategori B ON A.id_kategori=B.id_kategori
						INNER JOIN penerbit C ON A.id_penerbit=C.id_penerbit
						ORDER BY A.tahun_cetak DESC, A.created_date DESC
						limit $offset,$number");
	    	if($query->num_rows()>0){
	            return $query;
	        } else {
	            $query->free_result();
	            return $query;
	        }

	    }
    }
}