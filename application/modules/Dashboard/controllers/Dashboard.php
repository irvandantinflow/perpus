<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Dashboard_model');
		//if(!is_logged_in()){redirect('Inadminpage');}	
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Master',
	   		'content'	=> 'Dashboard/index',
	   		'newbook'	=> $this->Dashboard_model->newbook(),
	   		'newtesti'	=> $this->Dashboard_model->newtesti(),
	   		'page_action'		=> base_url('Student/do_login')
		);

	   	$this->load->view('index', $data);
	}

	public function katalog(){
		$this->load->library('pagination');
		$jumlah_data = $this->Dashboard_model->kataloglist()->num_rows();
		$config['base_url'] = base_url().'Dashboard/katalog/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 9;
		$config['attributes']['rel'] = FALSE;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li class="first">';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = 'First';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['prev_link'] = '<i class="fa fa-long-arrow-left"></i> Previous';
		$config['next_tag_open'] = '<li class="next">';
		$config['next_tag_close'] = '</li>';
		$config['next_link'] = 'Next <i class="fa fa-long-arrow-right"></i>';
		$config['cur_tag_open'] = '<h2>';
		$config['cur_tag_close'] = '</h2>';		
		$config['last_tag_open'] = '<li class="last">';
		$config['last_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$from = $this->uri->segment(3) > 0 ? $this->uri->segment(3) : 0 ;
		$this->pagination->initialize($config);

	   	$data = array(
	   		'active'	=> 'Master',
	   		'content'	=> 'Dashboard/katalog',
	   		'kataloglist'	=> $this->Dashboard_model->kataloglist($config['per_page'],$from),
	   		'page_action'		=> base_url('Student/do_login')
		);

	   	$this->load->view('katalog', $data);		
	}

	public function aboutus(){
	   	$data = array(
	   		'active'	=> 'Master',
	   		'content'	=> 'Dashboard/aboutus',
	   		'page_action'		=> base_url('Student/do_login')
		);

	   	$this->load->view('aboutus', $data);

	}

}