<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left"></div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $title; ?> </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form class="form-horizontal form-label-left" method="POST" action="<?php echo $page_action;?>" >

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Kode Anggota</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control" placeholder="Kode Anggota" name="kode_anggota" maxlength="10" value="<?php echo isset($get_data) ? $get_data->row()->kode_anggota : ''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Anggota</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control" placeholder="Nama Anggota" name="nama_anggota" maxlength="100" value="<?php echo isset($get_data) ? $get_data->row()->nama_anggota : ''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control has-feedback-left datepicker" id="dob" name="dob" placeholder="Tanggal Purchase" aria-describedby="inputSuccess2Status3" value="<?php echo isset($get_data) ? date('d-m-Y',strtotime($get_data->row()->dob)) : date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat Lahir</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control" placeholder="Tempat Lahir" name="pob" maxlength="50" value="<?php echo isset($get_data) ? $get_data->row()->pob : ''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Aktif</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <p>
                    YA <input type="radio" class="flat" name="aktif" value="Y" checked="" required <?php echo (isset($get_data) && $get_data->row()->aktif == 'Y') ? 'checked' : ''; ?>/> 
                    TIDAK <input type="radio" class="flat" name="aktif" value="N" <?php echo (isset($get_data) && $get_data->row()->aktif == 'N') ? 'checked' : ''; ?>/>
                  </p>
                </div>
              </div>

              <input type="hidden" name="id_anggota" value="<?php echo isset($get_data) ? $get_data->row()->id_anggota : ''; ?>"/>

              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                  <a href="<?php echo base_url('Anggota'); ?>" class="btn btn-primary" name="cancel">Cancel</a>
                  <button type="submit" class="btn btn-success" name="submit">Submit</button>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>