<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anggota_model extends CI_Model 
{    
	var $table = 'anggota';
	var $primary = 'id_anggota';

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT T0.*,
        							CASE T0.aktif WHEN 'Y' THEN 'YA' ELSE 'TIDAK' END AS status_aktif 
        							FROM anggota T0
        							order by T0.nama_anggota asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function save($id ='', $data =''){

    	if(!empty($data)){
    		$this->db->where($this->primary, $id);
			$is_check = $this->db->get($this->table);

			if($is_check->num_rows() > 0){

				$this->db->set($data);
				$this->db->set('updated_user', $this->session->userdata('user_id'));
				$this->db->set('updated_date', 'now()', FALSE);
				$this->db->where($this->primary, $id);
				
				$query = $this->db->update($this->table);
				if($query)
					return true;
				else
					return false;
			} else {
				$this->db->set('created_user', $this->session->userdata('user_id'));
				$this->db->set('created_date', 'now()', FALSE);
				$query = $this->db->insert($this->table, $data);
				if($query)
					return true;
				else
					return false;
			}
    	}  	
    }

	function checkID($id){
		$query = $this->db->get_where($this->table, array($this->primary=>$id));
		if ($query->num_rows() > 0) 
			return true;
		else
			return false;
    }

    public function edit($id){
		$query = $this->db->get_where($this->table, array($this->primary=>$id));
		return $query;
	}

    function cekDuplicateCode($kodeanggota = '', $id = ''){
		
		if($id != '')
			$query = $this->db->query("SELECT * FROM anggota WHERE kode_anggota = '" . $kodeanggota . "' AND id_anggota <> '" . $id . "'");
		else
			$query = $this->db->query("SELECT * FROM anggota WHERE kode_anggota = '" . $kodeanggota . "' ");
		
		if($query->num_rows() > 0){
			return false;
		}else{
			return true;	
		}
	}

	function getAnggotaAktif(){
        $query = $this->db->query("SELECT * from anggota where aktif = 'Y' order by nama_anggota asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function getAnggotaAll(){
        $query = $this->db->query("SELECT * from anggota order by nama_anggota asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

}   
