<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Anggota_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Anggota',
	   		'content'	=> 'Anggota/list_anggota',
	   		'title'		=> 'Anggota',
	   		'list'		=> $this->Anggota_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Anggota',
	   		'content'		=> 'Anggota/form_anggota',
		   	'title'			=> 'New Anggota',
	   		'page_action' 	=> base_url('Anggota/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){
		
		$this->form_validation->set_rules('kode_anggota','Kode Anggota', 'trim|required');
		$this->form_validation->set_rules('nama_anggota','Nama Anggota', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

        $this->form_validation->set_rules('kode_anggota','Kode Anggota', 'trim|required|callback_checkDuplicate');	

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Anggota');

		} else {
			
			$data = array(
						'kode_anggota'			=> $accept['kode_anggota'],
						'nama_anggota'			=> $accept['nama_anggota'],
						'dob'					=> isset($accept['dob']) ? date('Y-m-d', strtotime($accept['dob'])) : '',
						'pob'					=> $accept['pob'],
						'aktif'					=> $accept['aktif']);

			$query = $this->Anggota_model->save($accept['id_anggota'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Anggota');
		}	
	}

	function checkDuplicate($kodeanggota = '') { 

		$id = $this->input->post('id_anggota');
		$check = $this->Anggota_model->cekDuplicateCode($kodeanggota, $id);
		$this->form_validation->set_message('checkDuplicate', 'Data anggota untuk kode anggota : '.$kodeanggota.' sudah ada');
	
		if($check){
			return true;
		}else{
			return false;	
		}

	}

	function edit($id = 0){	
		if(!$this->Anggota_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Anggota');
		};

	   	$data = array(
	   		'active'		=> 'Anggota',
	   		'content'		=> 'Anggota/form_anggota',
	   		'title'			=> 'Edit Anggota',
	   		'page_action' 	=> base_url('Anggota/save'),
	   		'get_data'		=> $this->Anggota_model->edit($id),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}