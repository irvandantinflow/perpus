<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Review_model');
		$this->load->model('Buku/Buku_model');
		$this->load->model('Kategori/Kategori_model');
		$this->load->model('Penerbit/Penerbit_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Transaction',
	   		'content'	=> 'Review/list_review',
	   		'title'		=> 'Review',
	   		'list'		=> $this->Review_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function edit($id = 0){	
		if(!$this->Review_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Review');
		};

	   	$data = array(
	   		'active'		=> 'Transaction',
	   		'content'		=> 'Review/form_review',
	   		'title'			=> 'Edit Review',
	   		'page_action' 	=> base_url('Review/save'),
	   		'buku'			=> 	$this->Buku_model->getBukuAll(),
	   		'kategori'		=> 	$this->Kategori_model->getKategoriAll(),
		   	'penerbit'		=> 	$this->Penerbit_model->getPenerbitAll(),
	   		'get_data'		=> $this->Review_model->edit($id),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){

		$this->form_validation->set_rules('book_rate','Book Rating', 'trim|required');		

		$accept = $this->fungsi->accept_data(array_keys($_POST));	

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Review');

		} else {
			
			$data = array(
						'id_peminjaman_detail'		=> $accept['id_peminjaman_detail'],
						'id_buku'					=> $accept['id_buku'],
						'judul_buku'				=> $accept['judul_buku'],
						'book_rate'					=> $accept['book_rate'],
						'remarks'					=> $accept['remarks']);

			$query = $this->Review_model->save($data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Review');
		}	
	}

	

}