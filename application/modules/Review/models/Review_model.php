<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Review_model extends CI_Model 
{    
    var $table = 'review';
    var $primary = 'id_review';

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT T0.*, 
                                    T1.peminjaman_number,
                                    T1.tanggal_peminjaman,
                                    T2.book_rate,
                                    T2.remarks,
                                    T3.tahun_cetak,
                                    T4.kategori,
                                    T5.penerbit,
                                    T1.nama_anggota
                                    FROM peminjaman_detail T0
                                    INNER JOIN (SELECT A.*, B.nama_anggota FROM peminjaman_header A INNER JOIN anggota B ON A.id_anggota=B.id_anggota) T1 ON T0.id_peminjaman = T1.id_peminjaman
                                    LEFT JOIN review T2 ON T0.id_peminjaman_detail = T2.id_peminjaman_detail
                                    LEFT JOIN buku T3 ON T0.id_buku = T3.id_buku
                                    LEFT JOIN kategori T4 ON T3.id_kategori = T4.id_kategori
                                    LEFT JOIN penerbit T5 ON T3.id_penerbit = T5.id_penerbit
                                    order by T0.id_peminjaman_detail DESC");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function save($data =''){

        if(!empty($data)){

            $this->db->trans_start();

            $this->db->set('created_user', $this->session->userdata('user_id'));
            $this->db->set('created_date', 'now()', FALSE);
            $query = $this->db->insert($this->table, $data);
            if(!$query) return false;

            $sql = "SELECT * FROM peminjaman_detail WHERE id_peminjaman_detail = '".$data['id_peminjaman_detail']."' ";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $this->db->set('updated_user', $this->session->userdata('user_id'), FALSE);
                $this->db->set('updated_date', 'now()', FALSE);
                $this->db->set('review', 'Y'); 
                $this->db->where('id_peminjaman_detail', $data['id_peminjaman_detail']);
                $query = $this->db->update('peminjaman_detail');
                if(!$query) return false;
            }

            $this->db->trans_complete();

            return true;
        }   
    }

    function checkID($id){
        $query = $this->db->get_where('peminjaman_detail', array('id_peminjaman_detail'=>$id));
        if ($query->num_rows() > 0) 
            return true;
        else
            return false;
    }

    public function edit($id){
        $query = $this->db->query("SELECT T0.*, 
                                    T1.peminjaman_number,
                                    T1.tanggal_peminjaman,
                                    T2.book_rate,
                                    T2.remarks,
                                    T3.tahun_cetak,
                                    T4.id_kategori,
                                    T4.kategori,
                                    T5.id_penerbit,
                                    T5.penerbit
                                    FROM peminjaman_detail T0
                                    INNER JOIN peminjaman_header T1 ON T0.id_peminjaman = T1.id_peminjaman
                                    LEFT JOIN review T2 ON T0.id_peminjaman_detail = T2.id_peminjaman_detail
                                    LEFT JOIN buku T3 ON T0.id_buku = T3.id_buku
                                    LEFT JOIN kategori T4 ON T3.id_kategori = T4.id_kategori
                                    LEFT JOIN penerbit T5 ON T3.id_penerbit = T5.id_penerbit
                                    WHERE T0.id_peminjaman_detail = '".$id."' ");
        return $query;
    }

}   
