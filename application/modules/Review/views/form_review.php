<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left"></div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $title; ?> </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form class="form-horizontal form-label-left" method="POST" action="<?php echo $page_action;?>" >

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Peminjaman</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input readonly="" type="text" class="form-control has-feedback-left datepicker" id="tanggal_peminjaman" name="tanggal_peminjaman" placeholder="Tanggal Purchase" aria-describedby="inputSuccess2Status3" value="<?php echo isset($get_data) ? date('d-m-Y',strtotime($get_data->row()->tanggal_peminjaman)) : date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">No Peminjaman</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input readonly="" type="text" class="form-control" placeholder="No Peminjaman" name="peminjaman_number" value="<?php echo isset($get_data) ? $get_data->row()->peminjaman_number : ''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Buku</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="hidden" name="id_buku" value="<?php echo isset($get_data) ? $get_data->row()->id_buku : ''; ?>"/>
                  <input type="hidden" name="judul_buku" value="<?php echo isset($get_data) ? $get_data->row()->judul_buku : ''; ?>"/>
                  <select class="form-control select2_single" name="id_buku" disabled="">
                    <option value="">-- Select Buku --</option>
                    <?php foreach ($buku->result() as $baris) {
                      if (isset($get_data) && $baris->id_buku == $get_data->row()->id_buku) { ?>
                      <option value="<?php echo $baris->id_buku; ?>" selected ><?php echo strtoupper($baris->judul_buku); ?></option>
                    <?php } }?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <select class="form-control select2_single" name="id_kategori" disabled="">
                    <option value="">-- Select Kategori --</option>
                    <?php foreach ($kategori->result() as $baris) {
                      if (isset($get_data) && $baris->id_kategori == $get_data->row()->id_kategori) { ?>
                      <option value="<?php echo $baris->id_kategori; ?>" selected><?php echo strtoupper($baris->kategori); ?></option>
                    <?php } }?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Penerbit</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <select class="form-control select2_single" name="id_penerbit" disabled="">
                    <option value="">-- Select Penerbit --</option>
                    <?php foreach ($penerbit->result() as $baris) {
                      if (isset($get_data) && $baris->id_penerbit == $get_data->row()->id_penerbit) { ?>
                      <option value="<?php echo $baris->id_penerbit; ?>" selected><?php echo strtoupper($baris->penerbit); ?></option>
                    <?php } }?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun Cetak</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input readonly="" type="text" class="form-control" placeholder="Tahun Cetak" name="tahun_cetak" value="<?php echo isset($get_data) ? $get_data->row()->tahun_cetak : ''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Book Rating</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <?php if (isset($get_data) && $get_data->row()->review == 'Y'){ ?>
                    <div class="starrr stars-existing">
                      <?php for($i = 0; $i < $get_data->row()->book_rate; $i++){ ?>
                        <a href="#" class="fa fa-star"></a>
                      <?php }
                      for($i = 0; $i < (5 - $get_data->row()->book_rate); $i++){ ?>
                        <a href="#" class="fa fa-star-o"></a>
                      <?php } ?>
                    </div>
                    You gave a rating of <span class="stars-count-existing"><?php echo isset($get_data) ? $get_data->row()->book_rate : '0'; ?></span> star(s)
                    <input type="hidden" name="book_rate" id="book_rate" value="<?php echo isset($get_data) ? $get_data->row()->book_rate : '0'; ?>">
                  <?php } else { ?>
                    <div class="starrr stars"></div>
                      You gave a rating of <span class="stars-count">0</span> star(s)
                      <input type="hidden" name="book_rate" id="book_rate" value="">
                  <?php } ?>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Remarks</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <?php $readonly = isset($get_data) && $get_data->row()->review == 'Y' ? 'readonly' : ''; ?>
                  <textarea id="remarks" name="remarks" rows="3" class="form-control input-medium" <?php echo $readonly; ?> ><?php echo isset($get_data) ? $get_data->row()->remarks : ''; ?></textarea>
                </div>
              </div>              

              <input type="hidden" name="id_peminjaman_detail" value="<?php echo isset($get_data) ? $get_data->row()->id_peminjaman_detail : ''; ?>"/>

              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                  <a href="<?php echo base_url('Review'); ?>" class="btn btn-primary" name="cancel">Back</a>
                  <?php if (isset($get_data) && $get_data->row()->review == 'N'){ ?>
                    <button type="submit" class="btn btn-success" name="submit">Submit</button>
                  <?php } ?>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>