<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembalian extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Pengembalian_model');
		$this->load->model('Anggota/Anggota_model');
		$this->load->model('Peminjaman/Peminjaman_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Transaction',
	   		'content'	=> 'Pengembalian/list_pengembalian',
	   		'title'		=> 'Pengembalian',
	   		'list'		=> $this->Pengembalian_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add($id=0){	
		if($id){
			if(!$this->Peminjaman_model->checkID($id)){
				$this->session->set_flashdata('error', 'Data tidak ditemukan');
    			redirect('Pengembalian'); 
			}
			if(!$this->Peminjaman_model->checkBaseStatus($id)) {
                $this->session->set_flashdata('error', 'Status Document Sudah Close.');
    			redirect('Pengembalian'); 
            } 
		}

	   	$data = array(
	   		'active'			=> 'Transaction',
	   		'content'			=> 'Pengembalian/form_pengembalian',
		   	'title'				=> 'New Pengembalian',
		   	'anggota'			=> 	$this->Anggota_model->getAnggotaAll(),
		   	'page_action' 		=> base_url('Pengembalian/add_process'),
	   		'get_dataH'			=> $this->Peminjaman_model->edit($id),
	   		'get_dataD'			=> $this->Peminjaman_model->editDetailOpen($id)
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function add_process(){
		
		$data 	= $this->fungsi->accept_data(array_keys($_POST));

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'			=> $i,
				'id_peminjaman'		=> $row['id_peminjaman'],
				'base_line'			=> $row['base_line'],
				'id_buku'			=> $row['id_buku'],
				'judul_buku'		=> $row['judul_buku']
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'pengembalian_number'			=> $data['pengembalian_number'],
			'id_peminjaman'					=> $data['id_peminjaman'],
			'id_anggota'					=> $data['id_anggota'],
			'status'						=> 'O',
			'tanggal_pengembalian'			=> isset($data['tanggal_pengembalian']) ? date('Y-m-d', strtotime($data['tanggal_pengembalian'])) : '',
		);

		$query = $this->Pengembalian_model->addData($data_header, $data_detail);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Pengembalian');

	}

	function edit($id = 0){	
		if(!$this->Pengembalian_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Pengembalian');
		};

	   	$data = array(
	   		'active'			=> 'Transaction',
	   		'content'			=> 'Pengembalian/form_pengembalian',
	   		'title'				=> 'Edit Pengembalian',
	   		'page_action' 		=> base_url('Pengembalian/update_process'),
	   		'anggota'			=> 	$this->Anggota_model->getAnggotaAll(),
	   		'get_dataH'			=> $this->Pengembalian_model->edit($id),
	   		'get_dataD'			=> $this->Pengembalian_model->editdetail($id)
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function update_process(){
		
		$data 			= $this->fungsi->accept_data(array_keys($_POST));
		$idPinjam		= $data['id_pengembalian'];

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'					=> $i,
				'id_peminjaman'				=> $row['id_peminjaman'],
				'base_line'					=> $row['base_line'],
				'id_buku'					=> $row['id_buku'],
				'judul_buku'				=> $row['judul_buku'],
				'id_pengembalian_detail'	=> isset($row['id_pengembalian_detail']) ? $row['id_pengembalian_detail'] : 0,	
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'pengembalian_number'		=> $data['pengembalian_number'],
			'id_anggota'				=> $data['id_anggota'],
			'status'					=> $data['status'],
			'tanggal_pengembalian'		=> isset($data['tanggal_pengembalian']) ? date('Y-m-d', strtotime($data['tanggal_pengembalian'])) : '',	
		);

		$query = $this->Pengembalian_model->updateData($data_header, $data_detail, $idPinjam);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Pengembalian');

	}
}