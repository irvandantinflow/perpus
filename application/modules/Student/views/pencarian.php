<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left"></div>

      <div class="title_right">
        <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
          <div class="input-group"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $title; ?> </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form class="form-horizontal form-label-left" method="POST" action="<?php echo $page_action;?>" >

              <div class="form-group col-md-12">
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control has-feedback-left" id="pencarian" name="pencarian" placeholder="Search..." aria-describedby="inputSuccess2Status3" value="">
                  <span class="fa fa-search form-control-feedback left" aria-hidden="true"></span>
                </div>
                <div class="col-md-3">
                  <input type="submit" name="submit" value="Cari" class="btn btn-default">
                </div>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Advanced Search</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" style="display: none;">

                  </div>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>

      <?php if(!empty($list)) { ?>
      <div class="col-md-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Hasil Pencarian </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <ul class="messages">
            <?php 
            if($list != ''):
            foreach ($list as $val) {
              $text = $val->deskripsi_buku;
                            $num_char = strlen($text) > 100 ? 100 : strlen($text);
                            $cut_text = substr($text, 0, $num_char);
                            if ($text{$num_char - 1} != ' ') { // jika huruf ke 50 (50 - 1 karena index dimulai dari 0) buka  spasi
                                $new_pos = strrpos($cut_text, ' '); // cari posisi spasi, pencarian dari huruf terakhir
                                $cut_text = substr($text, 0, $new_pos);
                            }
                echo '<li>
                        <div class="left col-xs-1">
                        <img src="'.base_url().'uploads/'.$val->filename.'" height="100px" width="100px">
                        </div>
                        <div class="right col-sm-11">
                        <div class="message_date">
                          <h3 class="date text-info">'.$val->tahun_cetak.'</h3>
                          <!--p class="month">May</p-->
                        </div>
                        <div class="message_wrapper">
                          <h4 class="heading">'.$val->judul_buku.'</h4>
                          <blockquote class="message">'.$cut_text.'</blockquote>
                          <br />
                          <p class="url">
                            <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                            <a href="#"><i class="fa fa-paperclip"></i> '.$val->penerbit.' </a>
                          </p>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                      </li>';
                //echo $val->judul_buku;
              }
            ?>
              </ul>
              <div class="clearfix"></div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Recomendation Books</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" style="display: none;">
                    <table class="table">
                      <thead>
                        <tr>
                          <?php 
                          if($list != ''):
                            foreach ($recomend as $r) {
                              echo '<th>'.$r->judul_buku.'</th>';
                            }
                          endif;
                            ?>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            <?php
          endif;
            ?>
          </div>
        </div>
      </div>      
      <?php } ?>

    </div>
  </div>
</div>