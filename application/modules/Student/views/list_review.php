        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <?php echo $this->load->view('alert');?>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search"></div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>No Peminjaman</th>
                          <th>Tanggal Peminjaman</th>
                          <th>Judul Buku</th>
                          <th>Kategori</th>
                          <th>Penerbit</th>
                          <th>Tahun Cetak</th>
                          <th>Review</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php
                        $no = 1;
                        if ($list != '') :
                        foreach($list->result() as $row){ 
                        ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $row->peminjaman_number; ?></td>
                          <td><?php echo date('d-m-Y',strtotime($row->tanggal_peminjaman)); ?></td>
                          <td><?php echo $row->judul_buku; ?></td>
                          <td><?php echo $row->kategori; ?></td>
                          <td><?php echo $row->penerbit; ?></td>
                          <td><?php echo $row->tahun_cetak; ?></td>
                          <td>
                            <?php if($row->review == 'Y'){ ?>
                              <div class="starrr stars-existing">
                                <?php for($i = 0; $i < $row->book_rate; $i++){ ?>
                                  <a href="#" class="fa fa-star"></a>
                                <?php }
                                for($i = 0; $i < (5 - $row->book_rate); $i++){ ?>
                                  <a href="#" class="fa fa-star-o"></a>
                                <?php } ?>
                              </div>
                              </br>
                              You gave a rating of <span class="stars-count-existing"><?php echo $row->book_rate; ?></span> star(s)
                            <?php } ?>
                          </td>
                          <td>
                            <a class="glyphicon glyphicon-edit" href="<?php echo base_url();?>Student/Review/edit/<?php echo $row->id_peminjaman_detail;?>" title="Edit"></a>
                          </td>
                        </tr>
                        <?php } endif;?>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->