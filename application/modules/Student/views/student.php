        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <?php echo $this->load->view('alert');?>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search"></div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row top_tiles">

            </div>

          </div>
        </div>
        <!-- /page content -->