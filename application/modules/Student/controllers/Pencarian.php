<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pencarian extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Pencarian_model');
		if(!is_logged_in()){redirect('Dashboard');}	
		$this->load->library('Recommend');
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){  
		if(!is_logged_in()){redirect('Dashboard');}

		if(isset($_POST['submit'])){
			$accept = $this->fungsi->accept_data(array_keys($_POST));
			(object)$books = $this->Pencarian_model->getlistRecom();
			$recomend = $this->recommend->getRecommendations($books, $this->session->userdata("nama_anggota"));
		   	$list = $this->Pencarian_model->getlist($accept['pencarian']);
	   	}
	   	else{
	   		$recomend = null;
	   		$list = null;
	   	}
	   	$data = array(
	   		'active'			=> 'Rating',
	   		'content'			=> 'Student/Pencarian',
	   		'title'		=> 'Pencarian Buku',
	   		'recomend'	=> $recomend,
	   		'page_action' => 'Pencarian',
	   		'list' => $list
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

}