<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Student_model');
		//if(!is_logged_in()){redirect('Dashboard');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){  
		if(!is_logged_in()){redirect('Dashboard');}
	   	$data = array(
	   		'active'			=> 'Home',
	   		'content'			=> 'Student/student',
	   		'title'		=> 'Student'
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function do_login(){
		$user_id = $this->session->userdata('kode_anggota');
		
		if(!empty($user_id)){
			$this->session->set_flashdata('success', 'Anda sudah login');
			redirect('Student');
		};
		
		$this->Student_model->restrict(true);
		$this->form_validation->set_rules('kode_anggota', 'Username', 'trim|required');
		$this->form_validation->set_rules('dob', 'Password', 'trim|required');
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', 'Please input Username and/or Password');
			redirect('Dashboard');
		} else {
			$login = array(
				'kode_anggota'	=> $this->input->post('kode_anggota'),
			    'dob'	=> $this->input->post('dob')
			);
			$return = $this->Student_model->do_login($login);
			if($return){
				redirect('Student');
			} else {
				$this->session->set_flashdata('error', 'Invalid Username or Password');
				redirect('Dashboard');
			}
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('Dashboard');
	}
}