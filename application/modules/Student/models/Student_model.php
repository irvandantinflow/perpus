<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Student_model extends CI_Model 
{    
	var $table = 'anggota';
	var $primary = 'id_anggota';

    public function __construct(){
        parent::__construct(); 
    }

    function restrict($logged_out = FALSE){

		// If the user is logged in and he's trying to access a page
		// he's not allowed to see when logged in,
		// redirect him to the index!
		if ($logged_out && is_logged_in()){
	      	echo $this->fungsi->warning('Maaf, sepertinya Anda sudah login...',site_url());
	      	die();
		}
		
		// If the user isn' logged in and he's trying to access a page
		// he's not allowed to see when logged out,
		// redirect him to the login page!
		if ( ! $logged_out && !is_logged_in()){
		    echo $this->fungsi->warning('Anda diharuskan untuk Login bila ingin mengakses halaman ini.',site_url());
		    die();
		}
	}
	
	function do_login($login = NULL){

     	// A few safety checks
    	// Our array has to be set
    	if(!isset($login))
	        return FALSE;
		
    	//Our array has to have 2 values
     	//No more, no less!
    	if(count($login) != 2)
		return FALSE;
	
     	$username = $login['kode_anggota'];
     	$password = $login['dob'];

     	$sql    = "SELECT A.id_anggota, A.kode_anggota, A.nama_anggota, A.dob, A.pob,
     				A.aktif
                    FROM anggota A
                    WHERE A.aktif = 'Y' AND A.kode_anggota = '".$username."' AND A.dob = '".$password."'";

        $query=$this->db->query($sql);

     	foreach ($query->result() as $row){
    		$kode_anggota        	= $row->kode_anggota;
			$nama_anggota       	= $row->nama_anggota;
			$dob       	= $row->dob;
			$pob          	= $row->pob;
			$aktif          = $row->aktif;
			$id_anggota = $row->id_anggota;
    	}

     	if ($query->num_rows() == 1){
       	 	$newdata = array(
       	 		'id_anggota'	=> $id_anggota,
       	 	    'kode_anggota'		=> $kode_anggota,
              	'nama_anggota'  	=> $nama_anggota,
                'dob'  	=> $dob,
                'logged_in' 	=> TRUE,
		    	'pob'			=> $pob,
		    	'aktif'		=> $aktif
           		);
			// Our user exists, set session.
			$this->session->set_userdata($newdata);	  
			
			return TRUE;
		} else {
			// No existing user.
			return FALSE;
		}
	}

}   
