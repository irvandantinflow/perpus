<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pencarian_model extends CI_Model 
{    
	var $table = 'anggota';
	var $primary = 'id_anggota';

    public function __construct(){
        parent::__construct(); 
    }

    public function getlistRecom(){
    	$query = $this->db->query("select DISTINCT C.nama_anggota,B.judul_buku, D.rat
                                FROM review A 
                                INNER JOIN (SELECT (SUM(book_rate)/count(book_rate)) as rat, id_anggota, id_buku FROM review GROUP by id_buku, id_anggota) D on A.id_anggota=d.id_anggota AND A.id_buku = D.id_buku
                                INNER JOIN buku B on A.id_buku=B.id_buku
                                INNER JOIN anggota C ON a.id_anggota=C.id_anggota
                                ORDER by c.nama_anggota");
        if($query->num_rows()>0){
        	$books = array();
        	foreach ($query->result() as $row) {
        		if(!in_array($row->nama_anggota, $books, true)){
        			$books[$row->nama_anggota] = array();
	        	}
        	}

        	foreach ($query->result() as $row) {
        			$books[$row->nama_anggota] += array($row->judul_buku => $row->rat);
        	}
        	// die(var_dump($books));
            return $books;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function getlist($cari=""){
        if($cari != ""){
            $query = $this->db->query("SELECT T0.*, T1.kategori, T2.penerbit, T3.rat FROM buku T0
                        INNER JOIN kategori T1 ON T0.id_kategori=T1.id_kategori
                        INNER JOIN penerbit T2 ON T2.id_penerbit=T0.id_penerbit
                        LEFT JOIN (SELECT (SUM(book_rate)/count(book_rate)) as rat, id_anggota, id_buku FROM review GROUP by id_buku, id_anggota) T3 on T0.id_buku=T3.id_buku 
                        WHERE T0.judul_buku LIKE '%".$cari."%' OR T0.deskripsi_buku LIKE '%".$cari."%'
                        ORDER BY T3.rat");
            if($query->num_rows()>0){
                // die(var_dump($books));
                return $query->result();
            } else {
                $query->free_result();
                return '';
            }
        }
        else{
            return "";
        }
    }

}