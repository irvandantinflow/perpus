<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('User_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Master',
	   		'content'	=> 'User/list_user',
	   		'list'		=> $this->User_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'User/form_user',
		   	'title'			=> 'New User',
		   	'usergroup'		=> 	$this->User_model->getUsergroup(),
	   		'page_action' 	=> base_url('User/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){
		
		$this->form_validation->set_rules('fullname','Fullname', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

		if(empty($accept['user_id'])){
			$this->form_validation->set_rules('username','Username', 'trim|required|is_unique[user.username]');
		} else {
			$this->form_validation->set_rules('username','Username', 'trim|required');
		}

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('User');

		} else {
			if(empty($accept['user_id'])){

				if($accept['new_password'] != $accept['retype_password']){
					$this->session->set_flashdata('error', 'Password dan Re-type Password tidak sama');
					redirect('User');
				}

				$data = array(
						'username'			=> $accept['username'],
						'fullname'			=> $accept['fullname'],
						'password'			=> md5($accept['new_password']),
						'id_usergroup'		=> $accept['id_usergroup'],
						'aktif'				=> $accept['aktif']);

			} else {
				
				$data = array(
						'username'			=> $accept['username'],
						'fullname'			=> $accept['fullname'],
						'id_usergroup'		=> $accept['id_usergroup'],
						'aktif'				=> $accept['aktif']);

				if($accept['new_password'] != ''){
					if($accept['new_password'] != $accept['retype_password']){
						$this->session->set_flashdata('error', 'Password dan Re-type Password tidak sama');
						redirect('User');
					}
				
					$data += array( 'password' => md5($accept['new_password']) );
				}
			}

			$query = $this->User_model->save($accept['user_id'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('User');
		}	
	}

	public function delete($id){
		if (!$id) redirect('User');	
		$query = $this->User_model->delete($id);
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		else
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		
		redirect('User');
	}

	function edit($id = 0){	
		if(!$this->User_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('User');
		};

	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'User/form_user',
	   		'title'			=> 'Edit User',
	   		'page_action' 	=> base_url('User/save'),
	   		'usergroup'		=> $this->User_model->getUsergroup(),
	   		'get_data'		=> $this->User_model->edit($id),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}