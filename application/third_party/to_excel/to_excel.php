<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
* Excel library for Code Igniter applications
* Author: Federico Ram�rez a.k.a fedekun a.k.a lenkun - Feb 2010
*/

function to_excel($array, $filename='out', $title = '', $stream = false, $html='', $nocount='N'){
	header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename='.$filename.'.xls'); 

	if($stream) {
		echo $html;
	} else {
		echo $title;
		
		if(count($array) > 0 && $array != ''){
		
		$h = array();
		foreach($array as $row)
			foreach($row as $key=>$val)
				if(!in_array($key, $h))
					$h[] = $key;
		
		
			echo '<table border="1"><tr>';
			if($nocount == 'N') echo '<th>No. </th>';
			foreach($h as $key) {
				$key = ucwords($key);
				echo '<th>'.$key.'</th>';
			}
			echo '</tr>';
			
			$i = 1;
			foreach($array as $val)
				($nocount == 'N') ? _writeRow($val, $h, false, $i++) : _writeRow2($val, $h, false);
			
			echo '</table>';
		} else {
			echo '<table border="1"><tr>';
			echo '<th>No data available</th>';
			echo '</tr></table>';
			
		}
	}
}

function _writeRow($row, $h, $isHeader=false, $count){
    echo '<tr>';
    echo '<td>' . $count . '</td>';
    foreach($h as $r) {
        if($isHeader)	echo '<th>'.utf8_decode(@$row[$r]).'</th>';
        else			echo '<td>'.utf8_decode(@$row[$r]).'</td>';
    }
    echo '</tr>';
}


function _writeRow2($row, $h, $isHeader=false){
    echo '<tr>';
    foreach($h as $r) {
        if($isHeader)	echo '<th>'.utf8_decode(@$row[$r]).'</th>';
        else			echo '<td>'.utf8_decode(@$row[$r]).'</td>';
    }
    echo '</tr>';
}